
import json
from shutil import copyfile
from os import listdir
from os.path import isfile, join
from PyQt5 import QtWidgets,QtGui


from ScriptManager import *

class Model:
    def __init__(self):
        self.path='/sf/data/applications/BD-Optics/OpticsModels'
        self.scripts = ScriptManager()
        self.UIOrder = None
        self.UIVar = None
        self.UIScript = None
        self.UIName = None
        self.UIDesc = None
        self.new()

    def init(self,UIOrder,UIVar,UIScript,UIName,UIDesc):
        self.UIOrder=UIOrder
        self.UIVar=UIVar
        self.UIName=UIName
        self.UIDesc=UIDesc
        self.UIScript=UIScript
        self.displayScripts()
        self.new()

    def displayScripts(self):
        self.UIScript.clear()
        for ele in self.scripts.scripts:
            self.UIScript.addItem(ele)
        self.UIScript.setCurrentRow(0)

    def initScripts(self,UIEditor,UIEditorName):
        self.scripts.editor=UIEditor
        self.scripts.label=UIEditorName

    def eventScript(self,name):
        if 'Var' in name:
            self.changeVariable(name)
            return
        idx = 0
        if 'Script2' in name:
            idx =1
        elif 'Script3' in name:
            idx =2
        elif 'Script4' in name:
            idx =3
        if 'Add' in name:
            self.addScript(self.UIOrder[idx])
        elif 'Remove' in name:
            self.deleteScript(self.UIOrder[idx])
        else:
            self.moveScript(self.UIOrder[idx],name)

    def addScript(self,order):
        order.addItem(self.UIScript.currentItem().text())
        self.updateModel()

    def deleteScript(self,order):
        currentRow = order.currentRow()
        currentItem=order.takeItem(currentRow)
        self.updateModel()

    def moveScript(self,order,name):
        currentRow= order.currentRow()
        if currentRow == 0 and 'Up' in name:
            return
        if currentRow == order.count()-1 and 'Down' in name:
            return
        currentItem=order.takeItem(currentRow)
        if 'Down' in name:
            order.insertItem(currentRow+1,currentItem)
        else:
            order.insertItem(currentRow-1,currentItem)
        self.updateModel()

    def changeVariable(self,name):
        if 'Add' in name:
            idx =1
            while ('var%d' % idx) in self.variables.keys():
                idx+=1
            nrow = self.UIVar.rowCount()
            self.UIVar.insertRow(nrow)
            self.UIVar.setItem(nrow,0,QtWidgets.QTableWidgetItem('var%d' % idx))
            self.UIVar.setItem(nrow,1,QtWidgets.QTableWidgetItem('0.0'))
        else:
            currentRow= self.UIVar.currentRow() 
            self.UIVar.removeRow(currentRow)
        self.updateModel()
 
    def updateModel(self):
        self.Name = str(self.UIName.text())
        self.description = str(self.UIDesc.toPlainText())
        self.matchorder['Common'].clear()
        self.matchorder['Aramis'].clear()
        self.matchorder['Athos'].clear()
        self.matchorder['Porthos'].clear()
        for irow in range(self.UIOrder[0].count()):
            self.matchorder['Common'].append(str(self.UIOrder[0].item(irow).text()))
        for irow in range(self.UIOrder[1].count()):
            self.matchorder['Aramis'].append(str(self.UIOrder[1].item(irow).text()))
        for irow in range(self.UIOrder[2].count()):
            self.matchorder['Athos'].append(str(self.UIOrder[2].item(irow).text()))                              
        for irow in range(self.UIOrder[3].count()):
            self.matchorder['Porthos'].append(str(self.UIOrder[3].item(irow).text()))                              
        self.variables.clear()
        for i in range(self.UIVar.rowCount()):
            var = str(self.UIVar.item(i,0).text())
            val = float(str(self.UIVar.item(i,1).text()))
            self.variables[var]=val
    
    def updateDisplay(self):
        if self.UIName is None:
            return
        self.UIName.setText(self.Name)   
        self.UIDesc.clear()
        self.UIDesc.appendPlainText(self.description)
        for i in range(4):
            self.UIOrder[i].clear()
        for ele in self.matchorder['Common']:
            print(ele)
            self.UIOrder[0].addItem(ele)
        for ele in self.matchorder['Aramis']:
            print(ele)
            self.UIOrder[1].addItem(ele)
        for ele in self.matchorder['Athos']:
            print(ele)
            self.UIOrder[2].addItem(ele)
        for ele in self.matchorder['Porthos']:
            print(ele)
            self.UIOrder[3].addItem(ele)
        self.UIVar.setRowCount(0)
        icount = 0
        for key in self.variables.keys():
            self.UIVar.insertRow(icount)
            self.UIVar.setItem(icount,0,QtWidgets.QTableWidgetItem(key))
            self.UIVar.setItem(icount,1,QtWidgets.QTableWidgetItem('%f' % self.variables[key]))
            

    def new(self):
        self.saveName=None
        self.Name='New'
        self.description='To be written'
        self.matchorder = {'Common':[],'Aramis':[],'Athos':[],'Porthos':[]}
        self.variables = {}
        self.settings= {}
        self.optics = {'Aramis':{},'Athos':{},'Porthos':{}}
        self.updateDisplay()

    def save(self,filename):
        self.updateModel()  # get the current Model
        if filename is None:
            filename=self.saveName
        else:
            self.saveName=filename
        if filename is None:
            return
        data={}
        data['Name']=self.Name
        data['description']=self.description
        data['scripts']=self.matchorder
        data['variables']=self.variables
        data['settings']=self.settings
        data['optics']=self.optics
        with open(self.path+'/'+filename, 'w') as json_file:
            json.dump(data, json_file, indent=2)

    def load(self,filename):
        with open(self.path+'/'+filename, 'r') as json_file:
            data=json.load(json_file)
            self.saveName=filename
            self.Name=data['Name']
            self.matchorder=data['scripts']
            if not 'Porthos' in self.matchorder.keys():
                self.matchorder['Porthos']=[]
            self.variables=data['variables']
            if 'description' in data.keys():
                self.description=data['description']
            if 'settings' in data.keys():
                self.settings=data['settings']
            if 'optics' in data.keys():
                self.optics=data['optics']
        self.updateDisplay()



