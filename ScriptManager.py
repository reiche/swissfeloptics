import subprocess
from os import listdir, system
from shutil import copyfile
from os.path import isfile, join


class ScriptManager:
    def __init__(self):
        self.editor=None
        self.label=None
        self.path='/sf/data/applications/BD-Optics/OpticsScripts'
        self.scripts = []
        self.refreshList()
        self.isModified=False
        self.hasFile=False

    def refreshList(self):
        files = listdir(self.path)
        self.scripts.clear()
        for ele in files:
            if '.madx' in ele and '~' not in ele:
                self.scripts.append(ele)

    def newScript(self):
        name='newScript.madx'
        if name in self.scripts:
            idx=1
            while ('newScript%d.madx' % idx) in self.scripts:
                idx+=1
            name='newScript%d.madx' % idx
        self.label.setText(name)
        self.label.setStyleSheet("background-color : white")
        self.isModified = False
        self.hasFile = False
        self.editor.blockSignals(True)
        self.editor.clear()
        self.editor.blockSignals(False)


    def openScript(self,filename):
        with open(self.path+'/'+filename,'r') as f:
            self.label.setText(filename)
            self.label.setStyleSheet("background-color : white")
            self.isModified=False
            self.hasFile = True
            self.editor.blockSignals(True)
            self.editor.clear()
            for lines in f.readlines():
                self.editor.appendPlainText(lines.strip())
            self.editor.blockSignals(False)




    def edited(self):
        if self.isModified:
            return
        self.isModified=True
        self.label.setStyleSheet("background-color : yellow;")


    def saveScript(self,filename=None):
            if filename is None:
                filename=str(self.label.text())
            else:
                self.label.setText(filename)
            text=self.editor.toPlainText()
            with open(self.path+'/'+filename,'w') as f:
                f.write(text)
            self.hasFile=True
            self.isModified=False
            self.label.setStyleSheet("background-color : white")
            self.refreshList()





