
!--------------------------
! setting kicker and septum to zero for Linac 3 and Aramis
SYKICK:=0;
S20SY02.MKDC010.cory = SYKICK*2;
S20SY02.MKAC020.cory = SYKICK*3;
S20SY02.MKDC030.cory = SYKICK*2;
S20SY02.MKAC040.cory = SYKICK*3;
S20SY02.MKDC050.cory = SYKICK*2;
S20SY02.MBND200.angle = 0;

!------------------------------
! match periodic lattice of linac 3
s30cb01.mqua430.k1 := 0.8168941546;
s30cb02.mqua430.k1 := -0.8168914699;
muLin3=0.1883;

Linac3: SEQUENCE, REFER=centre, L=18.2;
   S30CB01, AT=4.55;
   S30CB02, AT=13.65;
ENDSEQUENCE;

use, sequence=linac3;

match,SEQUENCE=Linac3,range=#s/#e;
Vary,name=S30CB01.MQUA430.k1,step=0.0001;
Vary,name=S30CB02.MQUA430.k1,step=0.0001;
constraint,sequence=linac3,range=#e,mux=muLin3;
constraint,sequence=linac3,range=#e,muy=muLin3;
lmdif,calls=100,tolerance=1e-21;
endmatch;


if (initquads ==1){
   S30CB03.MQUA430.k1=S30CB01.MQUA430.k1;	
   S30CB04.MQUA430.k1=S30CB02.MQUA430.k1;
   S30CB05.MQUA430.k1=S30CB01.MQUA430.k1;
   S30CB06.MQUA430.k1=S30CB02.MQUA430.k1;
   S30CB07.MQUA430.k1=S30CB01.MQUA430.k1;
   S30CB08.MQUA430.k1=S30CB02.MQUA430.k1;
   S30CB09.MQUA430.k1=S30CB01.MQUA430.k1;
   S30CB10.MQUA430.k1=S30CB02.MQUA430.k1;
   S30CB11.MQUA430.k1=S30CB01.MQUA430.k1;
   S30CB12.MQUA430.k1=S30CB02.MQUA430.k1;
   S30CB13.MQUA430.k1=S30CB01.MQUA430.k1;
   S30CB14.MQUA430.k1=S30CB02.MQUA430.k1;
   S30CB15.MQUA430.k1=S30CB01.MQUA430.k1;
}

twiss,sequence=linac3;
nbetx=table(twiss,linac3$start,betx);
nalfx=table(twiss,linac3$start,alfx);
nbety=table(twiss,linac3$start,bety);
nalfy=table(twiss,linac3$start,alfy);


!-------------------------------
! From Switchyard into linac 3

if (initquads == 1){
   s20sy03.mqua020.k1 := 1.327299442;
   s20sy03.mqua030.k1 := -1.243913627;
   s20sy03.mqua050.k1 := 0.1681796673;
   s20sy03.mqua060.k1 := 1.017317117;
   s20sy03.mqua100.k1 := -1.161257642;
}

use,sequence=swissfel;
MATCH,SEQUENCE=swissfel,range=#s/s30cb02$start,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
VARY,NAME=s20sy03.mqua020.k1,STEP=0.0001;
VARY,NAME=s20sy03.mqua030.k1,STEP=0.0001;
VARY,NAME=s20sy03.mqua050.k1,STEP=0.0001;
VARY,NAME=s20sy03.mqua060.k1,STEP=0.0001;
VARY,NAME=s20sy03.mqua100.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=swissfel,range=s20sy03$end,betx=nbetx;
CONSTRAINT,SEQUENCE=swissfel,range=s20sy03$end,alfx=nalfx;
CONSTRAINT,SEQUENCE=swissfel,range=s20sy03$end,bety=nbety;
CONSTRAINT,SEQUENCE=swissfel,range=s20sy03$end,alfy=nalfy;
CONSTRAINT,SEQUENCE=swissfel,range=s20sy03.mqua060$start,betx<30; ! for the new design
LMDIF,CALLS=100,TOLERANCE=1.e-21;
ENDMATCH;
matching.linac3=TAR;


!-----------------------------
! match to referece point for emittance measurement
! position is s30cb10.mqua430

use,sequence=swissfel;
MATCH,SEQUENCE=swissfel,range=#s/sarcl01$start,betx=betax0,alfx=alphay0,bety=betay0,alfy=alphay0;
VARY,NAME=s30cb06.mqua430.k1,STEP=0.0001;
VARY,NAME=s30cb07.mqua430.k1,STEP=0.0001;
VARY,NAME=s30cb08.mqua430.k1,STEP=0.0001;
VARY,NAME=s30cb09.mqua430.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=swissfel,range=s30cb10.mqua430$start,betx=8.83;
CONSTRAINT,SEQUENCE=swissfel,range=s30cb10.mqua430$start,alfx=0.548;
CONSTRAINT,SEQUENCE=swissfel,range=s30cb10.mqua430$start,bety=30.44;
CONSTRAINT,SEQUENCE=swissfel,range=s30cb10.mqua430$start,alfy=-1.866;
LMDIF,CALLS=100,TOLERANCE=1.e-21;
ENDMATCH;

matching.endlinac3=TAR;

twiss,SEQUENCE=swissfel,range=#s/#e,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
plot,haxis=s,vaxis=betx,bety,range=S20sy01$START/S30cb15$end,colour=100;




!-----------------------------------
! collimator
if (initquads ==1){
   sarcl02.mqua130.k1=1.8;
}
use,sequence=sarcl02;
MATCH,RMATRIX,slow,SEQUENCE=sarcl02,range=#s/#e,betx=10,alfx=0,bety=10,alfy=0;
VARY,NAME=sarcl02.mqua130.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=sarcl02,range=sarcl02.mqua210,re56=R56*0.5;
LMDIF,CALLS=100,TOLERANCE=1.e-21;
ENDMATCH;

if (initquads ==1) {
   sarcl02.mqua210.k1=-sarcl02.mqua130.k1*0.9;
   sarcl02.mqua250.k1= sarcl02.mqua130.k1;
}

MATCH,SEQUENCE=sarcl02,range=#s/#e,betx=10,alfx=0,bety=10,alfy=0;
VARY,NAME=sarcl02.mqua250.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=Sarcl02,range=sarcl02.dbpm260$end,dpx=0;
LMDIF,CALLS=100,TOLERANCE=1.e-21;
ENDMATCH;


if (initquads ==1) {
   sarcl02.mqua310.k1=sarcl02.mqua250.k1;
   sarcl02.mqua350.k1=sarcl02.mqua210.k1;
   sarcl02.mqua460.k1=sarcl02.mqua130.k1;
}

dL=0.625+0.25;
bx0=25;
bx=bx0+dL*dL/bx0;
ax=-dL/bx0;
by0=0.8;
by=by0+dL*dL/by0;
ay=-dL/by0;


twiss,sequence=sarcl02,range=sarcl02.mqsk300/#e,betx=bx,alfx=ax,bety=by,alfy=ay;
nbetx= table(twiss,sarcl02$end,betx);
nalfx=-table(twiss,sarcl02$end,alfx);
nbety= table(twiss,sarcl02$end,bety);
nalfy=-table(twiss,sarcl02$end,alfy);


ecol_k2=110;
if (initquads ==1){
   sarcl02.msex255.k2:=ecol_k2;
   sarcl02.msex305.k2:=ecol_k2;
}

MATCH,chrom,SEQUENCE=sarcl02,range=#s/#e,betx=nbetx,alfx=nalfx,bety=nbety,alfy=nalfy;
VARY,NAME=ecol_k2,STEP=0.0001;
CONSTRAINT,SEQUENCE=Sarcl02,range=#e,ddx=0;
LMDIF,CALLS=100,TOLERANCE=1.e-21;
ENDMATCH;

sarcl02.msex255.k2=ecol_k2;
sarcl02.msex305.k2=ecol_k2;

twiss,chrom,rmatrix,sequence=sarcl02,range=#s/#e,betx=nbetx,alfx=nalfx,bety=nbety,alfy=nalfy;
plot,haxis=s,vaxis=betx,bety,colour=100;
plot,haxis=s,vaxis=dx,colour=100;
plot,haxis=s,vaxis=ddx,colour=100;
plot,haxis=s,vaxis=re56,colour=100;

!---------------------------------
! match into energy collimator
! reference point is sarma01-mqua010

if (initquads ==1){
   sarcl01.mqua020.k1 := -0.4550820417;
   sarcl01.mqua050.k1 := -0.02350776516;
   sarcl01.mqua080.k1 := -0.3705426893;
   sarcl01.mqua100.k1 := 0.3937186328;
   sarcl01.mqua140.k1 := 1.214406656;
   sarcl01.mqua190.k1 := -1.475278354;
}

use,sequence=swissfel;
MATCH,SEQUENCE=swissfel,range=#s/#e,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
VARY,NAME=sarcl01.mqua020.k1,STEP=0.0001;
VARY,NAME=sarcl01.mqua050.k1,STEP=0.0001;
VARY,NAME=sarcl01.mqua080.k1,STEP=0.0001;
VARY,NAME=sarcl01.mqua100.k1,STEP=0.0001;
VARY,NAME=sarcl01.mqua140.k1,STEP=0.0001;
VARY,NAME=sarcl01.mqua190.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=swissfel,range=sarma01.mqua010$start,betx=2.304;
CONSTRAINT,SEQUENCE=swissfel,range=sarma01.mqua010$start,alfx=-1.4045;
CONSTRAINT,SEQUENCE=swissfel,range=sarma01.mqua010$start,bety=25.929;
CONSTRAINT,SEQUENCE=swissfel,range=sarma01.mqua010$start,alfy=-6.363;
CONSTRAINT,SEQUENCE=swissfel,range=sarcl01.mqua050,bety<30;
CONSTRAINT,SEQUENCE=swissfel,range=sarcl01.mqua190,betx<30;
LMDIF,CALLS=100,TOLERANCE=1.e-21;
ENDMATCH;

matching.energycollimator=TAR;

twiss,sequence=swissfel,range=#s/#e,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
plot,haxis=s,vaxis=betx,bety,range=S30CB15$START/SARCL02$END,colour=100;


!---------------------
! match periodic lattice of Aramis


ARAMIS: SEQUENCE, REFER=centre, L=9.5;
SARUN01, AT=2.375;
SARUN02, AT=7.125;
ENDSEQUENCE;


sarun01.mqua080.k1 := -1.699673315;
sarun02.mqua080.k1 := 1.746074065;


use,sequence=aramis;

match,SEQUENCE=aramis,range=#s/#e;
Vary,name=sarun01.mqua080.k1,step=0.0001;
Vary,name=sarun02.mqua080.k1,step=0.0001;
constraint,sequence=aramis,range=#e,mux=muAR;
constraint,sequence=aramis,range=#e,muy=muAR;
lmdif,calls=100,tolerance=1e-21;
endmatch;


twiss,sequence=aramis;
nbetx=table(twiss,aramis$end,betx);
nalfx=table(twiss,aramis$end,alfx);
nbety=table(twiss,aramis$end,bety);
nalfy=table(twiss,aramis$end,alfy);

if (initquads == 1){
   sarma02.mqua050.k1=sarun01.mqua080.k1;
   sarma02.mqua120.k1=sarun02.mqua080.k1;
}


!---------------------------
! matching into the undulator line

if (initquads ==1){
   sarma01.mqua010.k1 := -1.435783623;
   sarma01.mqua060.k1 := 1.29993551;
   sarma01.mqua080.k1 := -0.1356922498;
   sarma01.mqua120.k1 := -0.4142170392;
   sarma01.mqua140.k1 := 0.4428419456;
}


use,sequence=swissfel;
MATCH,SEQUENCE=swissfel,range=#s/sarun01$start,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
VARY,NAME=sarma01.mqua010.k1,STEP=0.0001;
VARY,NAME=sarma01.mqua060.k1,STEP=0.0001;
VARY,NAME=sarma01.mqua080.k1,STEP=0.0001;
VARY,NAME=sarma01.mqua120.k1,STEP=0.0001;
VARY,NAME=sarma01.mqua140.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=swissfel,range=sarma02$start,betx=nbetx;
CONSTRAINT,SEQUENCE=swissfel,range=sarma02$start,alfx=nalfx;
CONSTRAINT,SEQUENCE=swissfel,range=sarma02$start,bety=nbety;
CONSTRAINT,SEQUENCE=swissfel,range=sarma02$start,alfy=nalfy;
CONSTRAINT,SEQUENCE=swissfel,range=sarma01.mqua060,betx<40;
LMDIF,CALLS=300,TOLERANCE=1.e-21;
ENDMATCH;

matching.undulator=TAR;


!------------------
! matching along undulator line


use,sequence=swissfel;
MATCH,SEQUENCE=swissfel,range=#s/sarun03$start,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
VARY,NAME=sarma02.mqua050.k1,STEP=0.0001;
VARY,NAME=sarma02.mqua120.k1,STEP=0.0001;
VARY,NAME=sarun01.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun02.mqua080.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=swissfel,range=sarun02$end,betx=nbetx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun02$end,alfx=nalfx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun02$end,bety=nbety;
CONSTRAINT,SEQUENCE=swissfel,range=sarun02$end,alfy=nalfy;
LMDIF,CALLS=300,TOLERANCE=1.e-21;
ENDMATCH;


if (initquads==1){
   sarun03.mqua080.k1=sarun01.mqua080.k1;
   sarun04.mqua080.k1=sarun02.mqua080.k1;
   sarun05.mqua080.k1=sarun01.mqua080.k1;
   sarun06.mqua080.k1=sarun02.mqua080.k1;
}


use,sequence=swissfel;
MATCH,SEQUENCE=swissfel,range=#s/sarun07$start,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
VARY,NAME=sarun03.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun04.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun05.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun06.mqua080.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=swissfel,range=sarun06$end,betx=nbetx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun06$end,alfx=nalfx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun06$end,bety=nbety;
CONSTRAINT,SEQUENCE=swissfel,range=sarun06$end,alfy=nalfy;
LMDIF,CALLS=300,TOLERANCE=1.e-21;
ENDMATCH;


if (initquads==1){
   sarun07.mqua080.k1=sarun05.mqua080.k1;
   sarun08.mqua080.k1=sarun06.mqua080.k1;
   sarun09.mqua080.k1=sarun05.mqua080.k1;
   sarun10.mqua080.k1=sarun06.mqua080.k1;
}

use,sequence=swissfel;
MATCH,SEQUENCE=swissfel,range=#s/sarun11$start,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
VARY,NAME=sarun07.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun08.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun09.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun10.mqua080.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=swissfel,range=sarun10$end,betx=nbetx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun10$end,alfx=nalfx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun10$end,bety=nbety;
CONSTRAINT,SEQUENCE=swissfel,range=sarun10$end,alfy=nalfy;
LMDIF,CALLS=300,TOLERANCE=1.e-21;
ENDMATCH;


if (initquads ==1){
   sarun11.mqua080.k1=sarun09.mqua080.k1;
   sarun12.mqua080.k1=sarun10.mqua080.k1;
   sarun13.mqua080.k1=sarun09.mqua080.k1;
   sarun14.mqua080.k1=sarun10.mqua080.k1;
}


use,sequence=swissfel;
MATCH,SEQUENCE=swissfel,range=#s/sarun15$start,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
VARY,NAME=sarun11.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun12.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun13.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun14.mqua080.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=swissfel,range=sarun14$end,betx=nbetx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun14$end,alfx=nalfx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun14$end,bety=nbety;
CONSTRAINT,SEQUENCE=swissfel,range=sarun14$end,alfy=nalfy;
LMDIF,CALLS=300,TOLERANCE=1.e-21;
ENDMATCH;


if (initquads ==1){
   sarun15.mqua080.k1=sarun13.mqua080.k1;
   sarun16.mqua080.k1=sarun14.mqua080.k1;
   sarun17.mqua080.k1=sarun13.mqua080.k1;
   sarun18.mqua080.k1=sarun14.mqua080.k1;
}
use,sequence=swissfel;
MATCH,SEQUENCE=swissfel,range=#s/sarun19$start,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
VARY,NAME=sarun15.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun16.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun17.mqua080.k1,STEP=0.0001;
VARY,NAME=sarun18.mqua080.k1,STEP=0.0001;
CONSTRAINT,SEQUENCE=swissfel,range=sarun18$end,betx=nbetx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun18$end,alfx=nalfx;
CONSTRAINT,SEQUENCE=swissfel,range=sarun18$end,bety=nbety;
CONSTRAINT,SEQUENCE=swissfel,range=sarun18$end,alfy=nalfy;
LMDIF,CALLS=300,TOLERANCE=1.e-21;
ENDMATCH;


if (initquads ==1){
   sarun19.mqua080.k1=sarun03.mqua080.k1;
   sarun20.mqua080.k1=sarun04.mqua080.k1;
   sarbd01.mqua020.k1=0;
}


match,sequence=swissfel,range=#s/#e,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
vary,name=sarun18.mqua080.k1,step=0.0001;
vary,name=sarun19.mqua080.k1,step=0.0001;
vary,name=sarun20.mqua080.k1,step=0.0001;
vary,name=sarbd02.mqua030.k1,step=0.0001;
constraint,sequence=swissfel,range=#e,betx<100;
constraint,sequence=swissfel,range=#e,bety<150;
constraint,sequence=swissfel,range=#e,dx<100;
constraint,sequence=swissfel,range=#e,dy<0.5;
lmdif,calls=300,tolerance=1e-21;
endmatch;


twiss,sequence=swissfel,range=#s/#e,betx=betax0,alfx=alphax0,bety=betay0,alfy=alphay0;
plot,haxis=s,vaxis=betx,bety,range=sarcl01$start/sarun20$end,colour=100;
plot,haxis=s,vaxis=betx,bety,range=#s/sarun20$end,colour=100;
plot,haxis=s,vaxis=betx,bety,range=#s/#e,colour=100;
plot,haxis=s,vaxis=dx,dy,range=#s/#e,colour=100;

