#!/bin/bash
export RPN_DEFNS=/afs/psi.ch/intranet/SF/Beamdynamics/.defns.rpn
echo $1

sddsmatchtwiss $1 SwissFEL_dist.sdds -xPlane=beta=30,alpha=0 -yPlane=beta=30,alpha=0
sddsanalyzebeam SwissFEL_dist.sdds dist.analyse
sdds2stream dist.analyse -col=enx,eny,betax,betay,alphax,alphay,St,Sdelta,pAverage > dist.txt 2>err.txt
echo Done



