#!/bin/bash
export RPN_DEFNS=/afs/psi.ch/intranet/SF/Beamdynamics/.defns.rpn
echo $1
elegant $1.ele
sddsplot $1.twi -col=s,beta[xy] -gra=line,vary -device=png > madx1.png
sddsplot $1.twi -col=s,pCentral0 -device=png > madx2.png
sddsplot $1.out -col=t,p -gra=dot -device=png > madx3.png
cp *.png ../MadXRun
