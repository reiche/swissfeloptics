#!/opt/gfa/python-3.5/latest/bin/python 

import sys
import datetime
import os.path
import time
from os import walk
import numpy as np
import sys
import re


from matplotlib.figure import Figure
import matplotlib.patches as patches

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)


 

class OpticsPlotting:
    def __init__(self,):
        print('Hello World - Optics Plotting')

    def initmpl(self,mplvl,mplwindow):
        self.fig=Figure()
        self.axes=self.fig.add_subplot(111)
        self.axes2 = self.axes.twinx()
        self.canvas = FigureCanvas(self.fig)
        mplvl.addWidget(self.canvas)
        self.canvas.draw()
        self.toolbar=NavigationToolbar(self.canvas,mplwindow, coordinates=True)
        mplvl.addWidget(self.toolbar)


    def plotSingle(self,x,y,color,legend,dashed=False):

        if dashed:    
            self.axes.plot(x,y,'--',color=color,label=legend)
        else:
            self.axes.plot(x,y,color=color,label=legend)




    def plot(self,data,filt,z0,z1):
        
        self.axes.clear()
        self.axes2.clear()

        s=np.array(data['S'])
        if z0>z1:
            tmp=z1
            z1=z0
            z0=tmp

        i1=np.argmin(np.abs(s-z0))
        i2=np.argmin(np.abs(s-z1))
        
        ylabel=r''
        if filt['BETX']:
            self.plotSingle(s[i1:i2],data['BETX'][i1:i2],(0,0,1,1),r'$\beta_{x}$')
            ylabel=ylabel+r'$\beta_x$ (m), '
        if filt['BETY']:
            self.plotSingle(s[i1:i2],data['BETY'][i1:i2],(1,0,0,1),r'$\beta_{y}$')
            ylabel=ylabel+r'$\beta_y$ (m), '
        if filt['ALFX']:
            self.plotSingle(s[i1:i2],data['ALFX'][i1:i2],(0,0,1,1),r'$\alpha_{x}$')
            ylabel=ylabel+r'$\alpha_x$ (rad), '
        if filt['ALFY']:
            self.plotSingle(s[i1:i2],data['ALFY'][i1:i2],(1,0,0,1),r'$\alpha_{y}$')
            ylabel=ylabel+r'$\alpha_y$ (rad), '
        if filt['DX']:
            self.plotSingle(s[i1:i2],data['DX'][i1:i2],(0,0,1,1),r'$\eta_{x}$')
            ylabel=ylabel+r'$\eta_x$ (m), '
        if filt['DY']:
            self.plotSingle(s[i1:i2],data['DY'][i1:i2],(1,0,0,1),r'$\eta_{y}$')
            ylabel=ylabel+r'$\eta_y$ (m), '
        if filt['RE56']:
            self.plotSingle(s[i1:i2],data['RE56'][i1:i2],(0,0,0,1),r'$R_{56}$')
            ylabel=ylabel+r'$R_{56}$ (m), '
        if filt['Energy']:
            self.plotSingle(s[i1:i2],data['Energy'][i1:i2],(0,1,0,1),r'$E$')
            ylabel=ylabel+r'$E$ (MeV), '


        if len(ylabel) < 3:
            self.canvas.draw()
            return

        self.axes.legend(bbox_to_anchor=(0.15,0.85))
        self.axes.set_xlabel('s (m)')
        self.axes.set_ylabel(ylabel[0:-2])


        self.plotLayout(s,data['NAME'])
        self.axes.set_xlim([s[i1],s[i2]])
        ylim=self.axes.get_ylim()
        dl=np.abs(ylim[1]-ylim[0])
        yl=[ylim[0],ylim[1]+0.2*dl]
        self.axes.set_ylim(yl)
        self.axes2.set_xlim([s[i1],s[i2]])
        self.canvas.draw()

        return



    def plotLayout(self,s,elements):
        splitquads=False
        sstart=0
        
        s1=[np.min(s),np.max(s)]
        s2=[0.9,0.9]
        self.axes2.plot(s1,s2,'k')
        for i,name in enumerate(elements):
            if 'MBND' in name:
                s1=s[i-1]
                s2=s[i]
                self.axes2.add_patch(patches.Rectangle((s1, 0.9), (s2-s1),0.03,facecolor='blue',edgecolor="none"))
            if 'MSEX' in name:
                s1=s[i-1]
                s2=s[i]
                self.axes2.add_patch(patches.Rectangle((s1, 0.87), (s2-s1),0.06,facecolor='green',edgecolor="none"))

            if 'UIND' in name:
                s1=s[i-1]
                s2=s[i]
                self.axes2.add_patch(patches.Rectangle((s1, 0.88), (s2-s1),0.04,facecolor='purple',edgecolor="none"))

            if 'ACC' in name or 'TDS' in name:
                s1=s[i-1]
                s2=s[i]
                self.axes2.add_patch(patches.Rectangle((s1, 0.89), (s2-s1),0.02,facecolor='cyan',edgecolor="none"))

            if 'MQUA' in name:
                if splitquads == True:
                    if 'END' in name:
                        s1=sstart
                        s2=s[i]
                        self.axes2.add_patch(patches.Rectangle((s1, 0.85), (s2-s1),0.1,facecolor='red',edgecolor="none"))
                        splitquads=False
                else:
                    if 'START' in name:
                        splitquads=True
                        sstart=s[i]
                    else:    
                        s1=s[i-1]
                        s2=s[i]
                        self.axes2.add_patch(patches.Rectangle((s1, 0.85), (s2-s1),0.1,facecolor='red',edgecolor="none"))

        self.axes2.set_ylim([0,1]) 
        self.axes2.yaxis.set_visible(False)
        return


        # get branch
        branches=['ARAMIS','ATHOS','INJECTOR']
        idx=self.ReferenceDestination.currentIndex()
        branch=branches[idx]

        # plotting range - catch the error that sometimes zeros are padded at the end
        self.n0=min(np.argwhere(self.s_elements>=self.smin))[0]
        idx=max(np.argwhere(self.s_elements > 0.01))[0]
        self.n1=max(np.argwhere(self.s_elements[0:idx]<=self.smax))[0]


        # update also optics and magnet table
        self.updateOpticsTable()
        self.updateMagnetTable()





#        self.updateFromServer()
        # update status since plot is called when it changes
#        self.StatusReferencePlot.setText(self.stref.get())
#        self.StatusLivePlot.setText(self.stlive.get())



        # live plot
        plotLive=self.actionLive.isChecked()

        valid=self.pvlive[branch]['VALID'].get()
        if valid is None or valid is 1:
            plotLive=False

        if plotLive:
            if self.actionBetax.isChecked():
                self.plotSingle(self.pvlive[branch]['BETAX'],(0,0,1,1),r'$\beta_{x}$')
            if self.actionBetay.isChecked():
                self.plotSingle(self.pvlive[branch]['BETAY'],(1,0,0,1),r'$\beta_{y}$')

            if self.actionAlphax.isChecked():
                self.plotSingle(self.pvlive[branch]['ALPHAX'],(0,0,1,1),r'$\alpha_{x}$')
            if self.actionAlphay.isChecked():
                self.plotSingle(self.pvlive[branch]['ALPHAY'],(1,0,0,1),r'$\alpha_{y}$')

            if self.actionMismatch.isChecked():
                self.plotMismatch(self.pvlive[branch],self.pvref[branch])

            if self.actionEtax.isChecked():
                self.plotSingle(self.pvlive[branch]['ETAX'],(0,1,1,1),r'$\eta_{x}$')
            if self.actionEtay.isChecked():
                self.plotSingle(self.pvlive[branch]['ETAY'],(1,1,0,1),r'$\eta_{y}$')
            if self.actionEta2x.isChecked():
                self.plotSingle(self.pvlive[branch]['ETA2X'],(0,1,1,1),r'$\eta^2_{x}$')
            if self.actionEta2y.isChecked():
                self.plotSingle(self.pvlive[branch]['ETA2Y'],(1,1,0,1),r'$\eta^2_{y}$')

            if self.actionX.isChecked():
                self.plotSingle(self.pvlive[branch]['X'],(0.5,0.5,1,1),r'$x}$')
            if self.actionY.isChecked():
                self.plotSingle(self.pvlive[branch]['Y'],(1,0.5,0.5,1),r'$y$')

            if self.actionMux.isChecked():
                self.plotSingle(self.pvlive[branch]['MUX'],(0.5,0.5,1,1),r'$\mu_{x}$')
            if self.actionMuy.isChecked():
                self.plotSingle(self.pvlive[branch]['MUY'],(1,0.5,0.5,1),r'$\mu_{y}$')

            if self.actionEnergy.isChecked():
                self.plotSingle(self.pvlive[branch]['ENERGY'],(0,1,0,1),r'$E$')
 

            if self.actionR11.isChecked():
                self.plotSingle(self.pvlive[branch]['R11'],(0,0,0,1),r'$R_{11}$')
            if self.actionR12.isChecked():
                self.plotSingle(self.pvlive[branch]['R12'],(0,0,0,1),r'$R_{12}$')
            if self.actionR21.isChecked():
                self.plotSingle(self.pvlive[branch]['R21'],(0,0,0,1),r'$R_{21}$')
            if self.actionR22.isChecked():
                self.plotSingle(self.pvlive[branch]['R22'],(0,0,0,1),r'$R_{22}$')
            if self.actionR33.isChecked():
                self.plotSingle(self.pvlive[branch]['R33'],(0,0,0,1),r'$R_{33}$')
            if self.actionR34.isChecked():
                self.plotSingle(self.pvlive[branch]['R34'],(0,0,0,1),r'$R_{34}$')
            if self.actionR43.isChecked():
                self.plotSingle(self.pvlive[branch]['R43'],(0,0,0,1),r'$R_{43}$')
            if self.actionR44.isChecked():
                self.plotSingle(self.pvlive[branch]['R44'],(0,0,0,1),r'$R_{44}$')
            if self.actionR56.isChecked():
                self.plotSingle(self.pvlive[branch]['R56'],(0,0,0,1),r'$R_{56}$')





        # reference plot
        plotref=self.actionReference.isChecked()

        valid=self.pvref[branch]['VALID'].get()
        if valid is None or valid is 1:
            plotref=False

        if plotref:
            if self.actionBetax.isChecked():
                self.plotSingle(self.pvref[branch]['BETAX'],(0,0,1,1),r'$\beta_{x,ref}$',plotLive)
            if self.actionBetay.isChecked():
                self.plotSingle(self.pvref[branch]['BETAY'],(1,0,0,1),r'$\beta_{y,ref}$',plotLive)

            if self.actionAlphax.isChecked():
                self.plotSingle(self.pvref[branch]['ALPHAX'],(0,0,1,1),r'$\alpha_{x,ref}$',plotLive)
            if self.actionAlphay.isChecked():
                self.plotSingle(self.pvref[branch]['ALPHAY'],(1,0,0,1),r'$\alpha_{y,ref}$',plotLive)

            if self.actionEtax.isChecked():
                self.plotSingle(self.pvref[branch]['ETAX'],(0,1,1,1),r'$\eta_{x,ref}$',plotLive)
            if self.actionEtay.isChecked():
                self.plotSingle(self.pvref[branch]['ETAY'],(1,1,0,1),r'$\eta_{y,ref}$',plotLive)
            if self.actionEta2x.isChecked():
                self.plotSingle(self.pvref[branch]['ETA2X'],(0,1,1,1),r'$\eta^2_{x,ref}$',plotLive)
            if self.actionEta2y.isChecked():
                self.plotSingle(self.pvref[branch]['ETA2Y'],(1,1,0,1),r'$\eta^2_{y,ref}$',plotLive)

            if self.actionX.isChecked():
                self.plotSingle(self.pvref[branch]['X'],(0.5,0.5,1,1),r'$x_{ref}$',plotLive)
            if self.actionY.isChecked():
                self.plotSingle(self.pvref[branch]['Y'],(1,0.5,0.5,1),r'$y_{ref}$',plotLive)

            if self.actionMux.isChecked():
                self.plotSingle(self.pvref[branch]['MUX'],(0.5,0.5,1,1),r'$\mu_{x,ref}$',plotLive)
            if self.actionMuy.isChecked():
                self.plotSingle(self.pvref[branch]['MUY'],(1,0.5,0.5,1),r'$\mu_{y,ref}$',plotLive)

            if self.actionEnergy.isChecked():
                self.plotSingle(self.pvref[branch]['ENERGY'],(0,1,0,1),r'$E_{ref}$',plotLive)
  
            if self.actionR11.isChecked():
                self.plotSingle(self.pvref[branch]['R11'],(0,0,0,1),r'$R_{11,ref}$',plotLive)
            if self.actionR12.isChecked():
                self.plotSingle(self.pvref[branch]['R12'],(0,0,0,1),r'$R_{12,ref}$',plotLive)
            if self.actionR21.isChecked():
                self.plotSingle(self.pvref[branch]['R21'],(0,0,0,1),r'$R_{21,ref}$',plotLive)
            if self.actionR22.isChecked():
                self.plotSingle(self.pvref[branch]['R22'],(0,0,0,1),r'$R_{22,ref}$',plotLive)
            if self.actionR33.isChecked():
                self.plotSingle(self.pvref[branch]['R33'],(0,0,0,1),r'$R_{33,ref}$',plotLive)
            if self.actionR34.isChecked():
                self.plotSingle(self.pvref[branch]['R34'],(0,0,0,1),r'$R_{34,ref}$',plotLive)
            if self.actionR43.isChecked():
                self.plotSingle(self.pvref[branch]['R43'],(0,0,0,1),r'$R_{43,ref}$',plotLive)
            if self.actionR44.isChecked():
                self.plotSingle(self.pvref[branch]['R44'],(0,0,0,1),r'$R_{44,ref}$',plotLive)
            if self.actionR56.isChecked():
                self.plotSingle(self.pvref[branch]['R56'],(0,0,0,1),r'$R_{56,ref}$',plotLive)

          
        # setting up the right labels
        ylabel=r''
        if self.actionBetax.isChecked():
            ylabel=ylabel+r'$\beta_x$ (m), '
        if self.actionBetay.isChecked():
            ylabel=ylabel+r'$\beta_y$ (m), '
        if self.actionAlphax.isChecked():
            ylabel=ylabel+r'$\alpha_x$ (rad), '
        if self.actionAlphay.isChecked():
            ylabel=ylabel+r'$\alpha_y$ (rad), '
        if self.actionMismatch.isChecked():
            ylabel=ylabel+r'$\zeta_x$, '
            ylabel=ylabel+r'$\zeta_y$, '
        if self.actionEtax.isChecked():
            ylabel=ylabel+r'$\eta_x$ (m), '
        if self.actionEtay.isChecked():
            ylabel=ylabel+r'$\eta_y$ (m), '
        if self.actionEta2x.isChecked():
            ylabel=ylabel+r'$\eta^2_x$ (m), '
        if self.actionEta2y.isChecked():
            ylabel=ylabel+r'$\eta^2_y$ (m), '
        if self.actionX.isChecked():
            ylabel=ylabel+r'$x$ (m), '
        if self.actionY.isChecked():
            ylabel=ylabel+r'$y$ (m), '
        if self.actionMux.isChecked():
            ylabel=ylabel+r'$\mu_x$ (rad), '
        if self.actionMuy.isChecked():
            ylabel=ylabel+r'$\mu_y$ (rad), '
        if self.actionEnergy.isChecked():
            ylabel=ylabel+r'$E$ (MeV), '
        if self.actionR11.isChecked():
            ylabel=ylabel+r'$R_{11}$, '
        if self.actionR12.isChecked():
            ylabel=ylabel+r'$R_{12}$, '
        if self.actionR21.isChecked():
            ylabel=ylabel+r'$R_{21}$, '
        if self.actionR22.isChecked():
            ylabel=ylabel+r'$R_{22}$, '
        if self.actionR33.isChecked():
            ylabel=ylabel+r'$R_{33}$, '
        if self.actionR34.isChecked():
            ylabel=ylabel+r'$R_{34}$, '
        if self.actionR43.isChecked():
            ylabel=ylabel+r'$R_{43}$, '
        if self.actionR44.isChecked():
            ylabel=ylabel+r'$R_{44}$, '
        if self.actionR56.isChecked():
            ylabel=ylabel+r'$R_{56}$, '

        if len(ylabel) < 3: 
            self.canvas.draw()
            return
        if plotLive is False and plotref is False:
            self.canvas.draw()
            return

        self.axes.legend(bbox_to_anchor=(1, 0.845))
        self.axes.set_xlabel('s (m)')
        self.axes.set_ylabel(ylabel[0:-2])
        self.axes.set_xlim(self.smin,self.smax)


        # do the element drawing

        splitquads=False
        sstart=0
        
        s1=[0,self.s_elements[self.n1-1]]
        s2=[0.9,0.9]
        self.axes2.plot(s1,s2,'k')
        for i in range(1,self.n1):
            name=self.elements[i]
            if 'MBND' in name:
                s1=self.s_elements[i-1]
                s2=self.s_elements[i]
                self.axes2.add_patch(patches.Rectangle((s1, 0.9), (s2-s1),0.03,facecolor='blue',edgecolor="none"))

            if 'MSEX' in name:
                s1=self.s_elements[i-1]
                s2=self.s_elements[i]
                self.axes2.add_patch(patches.Rectangle((s1, 0.87), (s2-s1),0.06,facecolor='green',edgecolor="none"))

            if 'UIND' in name:
                s1=self.s_elements[i-1]
                s2=self.s_elements[i]
                self.axes2.add_patch(patches.Rectangle((s1, 0.88), (s2-s1),0.04,facecolor='purple',edgecolor="none"))

            if 'ACC' in name or 'TDS' in name:
                s1=self.s_elements[i-1]
                s2=self.s_elements[i]
                self.axes2.add_patch(patches.Rectangle((s1, 0.89), (s2-s1),0.02,facecolor='cyan',edgecolor="none"))

            if 'MQUA' in name:
                if splitquads == True:
                    if 'END' in name:
                        s1=sstart
                        s2=self.s_elements[i]
                        self.axes2.add_patch(patches.Rectangle((s1, 0.85), (s2-s1),0.1,facecolor='red',edgecolor="none"))
                        splitquads=False
                else:
                    if 'START' in name:
                        splitquads=True
                        sstart=self.s_elements[i]
                    else:    
                        s1=self.s_elements[i-1]
                        s2=self.s_elements[i]
                        self.axes2.add_patch(patches.Rectangle((s1, 0.85), (s2-s1),0.1,facecolor='red',edgecolor="none"))


        self.axes2.set_ylim([0,1]) 
        self.axes2.yaxis.set_visible(False)
        self.axes2.set_xlim(self.smin,self.smax)


        self.canvas.draw()


 
#----------------
# general interface with optics server


    def updateFromServer(self):
        
        self.epicsupdate=False
        time.sleep(0.7)
        self.ReferenceModel.blockSignals(True)
        self.ReferenceModel.setCurrentIndex(self.mode.get())
        self.ReferenceModel.blockSignals(False)

        self.CustomSARCL.blockSignals(True)
        self.CustomSARCL.setCurrentIndex(self.cusSARCL_ref.get())
        self.CustomSARCL.blockSignals(False)

  

#------------
# export lattice

    def export(self):
        
        sender=str(self.sender().objectName())
        if 'Madx' in sender:
            madx=True
            title="Export Madx Lattice"
            filt ="Lattice files (*.madx)"
            tag1='MADX'
        else:
            madx=False
            title="Export Elegant Lattice"
            filt ="Lattice files (*.lat)"
            tag1='ELEGANT'

        if 'Reference' in sender:
            tag2='REF'
        else:
            tag2='LIVE'


        filename = QtGui.QFileDialog.getSaveFileName(self,title,'',filt)
        if filename=='':
            return

        line=str(self.ReferenceDestination.currentText()).upper()

        
        PV='OPTICSSERVER:EXPORT-%s-%s-%s' % (tag1,line,tag2)        
        caput(PV,filename.encode())




#-------------
# all Tabs
    def tabChanged(self,idx):
        if idx is 1:
            self.updateOpticsTable()
        if idx is 2:
            self.updateMagnetTable()


 #--------------------------------------
# Tab Panel 1 - initiate new matching etc


    def initiateLiveTracking(self):
        self.pvbx0.put(float(str(self.bx0.text())))
        self.pvby0.put(float(str(self.by0.text())))
        self.pvax0.put(float(str(self.ax0.text())))
        self.pvay0.put(float(str(self.ay0.text())))
        self.inittrack.put(1)

    def initiateLiveUpdating(self):
        self.pvbx0.put(float(str(self.bx0.text())))
        self.pvby0.put(float(str(self.by0.text())))
        self.pvax0.put(float(str(self.ax0.text())))
        self.pvay0.put(float(str(self.ay0.text())))
        self.initupdate.put(1)
    

    def setMatchingPointFromReference(self):
        self.refloc.put(1)  # update the betas and alphas from referenc emodel
        time.sleep(1)
        self.getMatchingPoint()


    def setMatchingPoint(self):
        print('Writing new location')
        self.loc.put(str(self.Location.text()).encode())       # write the location name for reference
        time.sleep(1)
        self.Location.setText(self.loc.get())


    def getMatchingPoint(self):
        self.bx0.setText('%8.3f' % float(self.pvbx0.get()))
        self.ax0.setText('%8.3f' % float(self.pvax0.get()))
        self.by0.setText('%8.3f' % float(self.pvby0.get()))
        self.ay0.setText('%8.3f' % float(self.pvay0.get()))
        self.Location.setText(self.loc.get())


    def changeMode(self):
        self.mode.put(self.ReferenceModel.currentIndex())


    def custom(self):
         sender=str(self.sender().objectName())    
         if 'SARCL' in sender:
             self.cusSARCL_ref.put(self.CustomSARCL.currentIndex())


#-------------------------------------
# Tab Panel 3 - Show Magnet Functions
 

    def writeoutMagnetTable(self):
        for i in range(self.QuadTable.rowCount()):
            name=str(self.QuadTable.item(i,0).text())
            val =float(self.QuadTable.item(i,1).text())
            nameOM=name.replace('-','.') # switch to the online model convention
            if '.MQ' in nameOM:
                kl=self.calMagnet.QuadrupoleK2KL(nameOM,val,'P')
                caput('%s:K1L-SET' % name,kl)
            if '.MS' in nameOM:
                kl=self.calMagnet.SextupoleK2KL(nameOM,val,'P')
                caput('%s:K2L-SET' % name,kl)
            if '.MB' in nameOM:
                if 'MBND100' in name or 'SATSY01' in name:
                    caput('%s:K0L-SET' % name,val)
       
    def updateMagnetTable(self):


        regexp='.*'+self.QTRegExp.text()+'.*'
        regexpstart='.*'+self.QTStart.text()+'.*'
        regexpend='.*'+self.QTEnd.text()+'.*'

        dodiff=self.QTDiff.isChecked()
        dobend=self.QTDipole.isChecked()
        doquad=self.QTQuadrupole.isChecked()
        dosext=self.QTSextupole.isChecked()

        tol=float(self.QTTol.text())/100.

        hasStarted=False
        hasEnded=False

        self.QuadTable.clear()
        self.QuadTable.setRowCount(0)
        self.QuadTable.setColumnCount(3)
        self.QuadTable.setHorizontalHeaderItem(0,QtGui.QTableWidgetItem("Element"))
        self.QuadTable.setHorizontalHeaderItem(1,QtGui.QTableWidgetItem("Reference"))
        self.QuadTable.setHorizontalHeaderItem(2,QtGui.QTableWidgetItem("Live"))


        branches=['ARAMIS','ATHOS','INJECTOR']
        idx=self.ReferenceDestination.currentIndex()
        branch=branches[idx]


        ele=bytes(self.pvref[branch]['MAGNET'].get()).decode().split(';')   
        ele_live=bytes(self.pvlive[branch]['MAGNET'].get()).decode().split(';')    

        ns=len(ele)
        idx=0
        for i in range(0,ns):
            if ':' in ele[i]:
                comp=ele[i].split(':')
                name=comp[0]
                val=float(comp[1])
                comp=ele_live[i].split(':')
                name_live=comp[0]
                val_live=float(comp[1])

                if not hasStarted:
                    res=re.match(regexpstart,str(name))
                    if res is not None:
                        hasStarted=True

                addLine=hasStarted
                res=re.match(regexp,str(name))
                if res is None:
                    addLine=False
                if '-MBND' in name and not dobend:
                    addLine=False
                if '-MQ' in name and not doquad:
                    addLine=False
                if '-MSEX' in name and not dosext:
                    addLine=False
                tollev=max([np.abs(val)*tol,0.001])
                if dodiff and (np.abs(val-val_live) < tollev):
                    addLine=False
                    
                if addLine:
                    self.QuadTable.insertRow(idx)
                    self.QuadTable.setItem(idx,0,QtGui.QTableWidgetItem(name))
                    self.QuadTable.setItem(idx,1,QtGui.QTableWidgetItem(str('%9.4f' % val)))
                    self.QuadTable.item(idx,1).setBackground(QtGui.QColor(255,255,200))
                    self.QuadTable.setItem(idx,2,QtGui.QTableWidgetItem(str('%9.4f' % val_live)))
                    self.QuadTable.item(idx,2).setBackground(QtGui.QColor(200,255,255))
                    idx=idx+1
        self.QuadTable.resizeColumnsToContents()    
        return    



#--------------------------------------
# Tab Pabel 2 - Show Optics Functions
        
    def updateOpticsTable(self):


        # get branch
        branches=['ARAMIS','ATHOS','INJECTOR']
        idx=self.ReferenceDestination.currentIndex()
        branch=branches[idx]


        self.OpticsTableRef.clear()
        self.OpticsTableRef.setRowCount(0)
        self.OpticsTableRef.setColumnCount(12)
        self.OpticsTableRef.setHorizontalHeaderItem(0,QtGui.QTableWidgetItem("Element"))
        self.OpticsTableRef.setHorizontalHeaderItem(1,QtGui.QTableWidgetItem("  s (m)  "))
        self.OpticsTableRef.setHorizontalHeaderItem(2,QtGui.QTableWidgetItem("Energy (MeV)"))
        self.OpticsTableRef.setHorizontalHeaderItem(3,QtGui.QTableWidgetItem(" betax (m) "))
        self.OpticsTableRef.setHorizontalHeaderItem(4,QtGui.QTableWidgetItem("  alphax  "))
        self.OpticsTableRef.setHorizontalHeaderItem(5,QtGui.QTableWidgetItem(" betay (m) "))
        self.OpticsTableRef.setHorizontalHeaderItem(6,QtGui.QTableWidgetItem("  alphay  "))
        self.OpticsTableRef.setHorizontalHeaderItem(7,QtGui.QTableWidgetItem("Energy (MeV)"))
        self.OpticsTableRef.setHorizontalHeaderItem(8,QtGui.QTableWidgetItem(" betax (m) "))
        self.OpticsTableRef.setHorizontalHeaderItem(9,QtGui.QTableWidgetItem("  alphax  "))
        self.OpticsTableRef.setHorizontalHeaderItem(10,QtGui.QTableWidgetItem(" betay (m) "))
        self.OpticsTableRef.setHorizontalHeaderItem(11,QtGui.QTableWidgetItem("  alphay  "))

        erg=self.pvref[branch]['ENERGY'].get()
        bx =self.pvref[branch]['BETAX'].get()
        ax =self.pvref[branch]['ALPHAX'].get()
        by =self.pvref[branch]['BETAY'].get()
        ay =self.pvref[branch]['ALPHAY'].get()

        erg_l=self.pvlive[branch]['ENERGY'].get()
        bx_l =self.pvlive[branch]['BETAX'].get()
        ax_l =self.pvlive[branch]['ALPHAX'].get()
        by_l =self.pvlive[branch]['BETAY'].get()
        ay_l =self.pvlive[branch]['ALPHAY'].get()


        regexp='.*'+self.OTRegExp.text()+'.*'
        ns=len(self.s_elements)
        idx=0
        for i in range(0,ns):
            res=re.match(regexp,self.elements[i])
            if res!=None:
                self.OpticsTableRef.insertRow(idx)
                self.OpticsTableRef.setItem(idx,0,QtGui.QTableWidgetItem(self.elements[i]))
                self.OpticsTableRef.setItem(idx,1,QtGui.QTableWidgetItem(str('%8.3f' % self.s_elements[i])))
                self.OpticsTableRef.setItem(idx,2,QtGui.QTableWidgetItem(str('%8.3f' % erg[i])))
                self.OpticsTableRef.setItem(idx,3,QtGui.QTableWidgetItem(str('%8.3f' % bx[i])))
                self.OpticsTableRef.setItem(idx,4,QtGui.QTableWidgetItem(str('%8.3f' % ax[i])))
                self.OpticsTableRef.setItem(idx,5,QtGui.QTableWidgetItem(str('%8.3f' % by[i])))
                self.OpticsTableRef.setItem(idx,6,QtGui.QTableWidgetItem(str('%8.3f' % ay[i])))
                self.OpticsTableRef.setItem(idx,7,QtGui.QTableWidgetItem(str('%8.3f' % erg_l[i])))
                self.OpticsTableRef.setItem(idx,8,QtGui.QTableWidgetItem(str('%8.3f' % bx_l[i])))
                self.OpticsTableRef.setItem(idx,9,QtGui.QTableWidgetItem(str('%8.3f' % ax_l[i])))
                self.OpticsTableRef.setItem(idx,10,QtGui.QTableWidgetItem(str('%8.3f' % by_l[i])))
                self.OpticsTableRef.setItem(idx,11,QtGui.QTableWidgetItem(str('%8.3f' % ay_l[i])))
                for j in range(2,7):
                    self.OpticsTableRef.item(idx,j).setBackground(QtGui.QColor(255,255,200))
                    self.OpticsTableRef.item(idx,j+5).setBackground(QtGui.QColor(200,255,255))
                idx=idx+1
        self.OpticsTableRef.resizeColumnsToContents()    
        return    


#----------------

    def getLayout(self):

        branches=['ARAMIS','ATHOS','INJECTOR']
        idx=self.ReferenceDestination.currentIndex()
        branch=branches[idx]

        self.s_elements=self.pvref[branch]['S'].get()     
        eles=self.pvref[branch]['ELEMENT'].get()
        if self.s_elements is None or eles is None:
            self.hasLayout=False
        else:
            self.elements=bytes(eles).decode().split(';')
            self.hasLayout=True
            self.getPlotRange()



    def getPlotRange(self):
        start=str(self.PlotStart.text()).upper().replace('-','.')
        end  =str(self.PlotEnd.text()).upper().replace('-','.')
        istart=-1
        iend=-1
        for i in range(0,len(self.elements)):
            ele=self.elements[i]
            if ele[0] is 'S':
                if istart<0 and start in ele[0:7]:
                    istart=i
                if end in ele[0:7]:
                    iend=i
        if istart<0:
            self.smin=0
        else:
            self.smin=self.s_elements[istart]

        if iend<0:
            self.smax=max(self.s_elements)
        else:
            self.smax=self.s_elements[iend]

        if self.smin>=self.smax:
            self.smin=0
            istart=-1

        # error checking
        if istart<0:
            self.PlotStart.setText('SINLH01')
        if iend<0:
            self.PlotEnd.setText(self.elements[-2][0:7])

        self.doPlot=True


#------------
# plotting interface

    def plotMismatch(self,PV1,PV2):
        bx1=PV1['BETAX'].get()
        by1=PV1['BETAY'].get()
        ax1=PV1['ALPHAX'].get()
        ay1=PV1['ALPHAY'].get()
        bx2=PV2['BETAX'].get()
        by2=PV2['BETAY'].get()
        ax2=PV2['ALPHAX'].get()
        ay2=PV2['ALPHAY'].get()
        
        # derive value
        gx1=(1+ax1*ax1)/bx1
        gx2=(1+ax2*ax2)/bx2
        gy1=(1+ay1*ay1)/by1
        gy2=(1+ay2*ay2)/by2

        misx=0.5*(gx1*bx2+gx2*bx1-2*ax1*ax2)
        misy=0.5*(gy1*by2+gy2*by1-2*ay1*ay2)

        n0=self.n0
        n1=self.n1
        if len(misx)<n1:
            n1=len(misx)
        if n0>n1:
            n0=0
        self.axes.plot(self.s_elements[n0:n1],misx[n0:n1],color=(0,0,1,1),label=r'$\zeta_x$')
        self.axes.plot(self.s_elements[n0:n1],misy[n0:n1],color=(1,0,0,1),label=r'$\zeta_y$')









#---------------
# Timer event check whether some plotting has to be done

    def requestEPICSUpdate(self,pvname=None,**kw):
        self.epicsupdate=True

    def newOpticsAvailable(self,pvname=None,char_value=None,**kw):   
        self.doPlot=True

    def enforcePlot(self):
        self.doPlot=True


    def QTimerEvent(self):     # periodic update of system variables
        self.StatusPing.setText(self.stping.get())
        if self.epicsupdate:
            self.updateFromServer()

        if self.doPlot:
            self.plot()

# --------------------------------
# Main routine

if __name__ == '__main__':
    QtGui.QApplication.setStyle(QtGui.QStyleFactory.create("plastique"))
    app=QtGui.QApplication(sys.argv)
    main=OpticsClient()
    main.show()
    sys.exit(app.exec_())
