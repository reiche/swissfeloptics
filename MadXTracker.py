import sys
import numpy as np
from subprocess import PIPE, Popen, run
from os import listdir, remove, chdir, system
from os.path import isfile, join

import tempfile

#import paramiko

from PyQt5.QtCore import QTimer, pyqtSignal, QObject

# search path for online model
sys.path.append('/sf/bd/applications/OnlineModel/current')

from OMAppTemplate import ApplicationTemplate
from OMMadxLatticeLight import *
import OMFacility
import OMElegant as Elegant
import OMDistribution as Distribution
import OMMachineInterface

class MadXTracker(ApplicationTemplate, QObject):  # use the default event handler to parse through line

    trackingDone = pyqtSignal(int)

    def __init__(self, log = None,version = 0):

        super(QObject, self).__init__()
        super(MadXTracker, self).__init__()

        self.elegant = Elegant.Elegant()
        self.dist = None

        self.p = None
        self.line = None
        self.dest = None
        self.Log = log
        self.Facility = OMFacility.Facility(1, version)  # initialize the lattice
        self.Facility.forceEnergyAt('SINLH01.DBAM010', 140e6)
        self.MI=OMMachineInterface.MachineInterface()
        if version == 0:
            print('collecting EPICS Channels')
            self.Facility.writeFacility(self.MI)

        self.Madx = MadXLattice()
        self.setLine(0)
#        self.root='/sf/data/applications/BD-Optics'
        self.root='/sf/bd/applications/Optics/current'
        self.path=self.root+'/MadXRun'
#        self.elepath='/afs/psi.ch/intranet/SF/Beamdynamics/Sven/BD_Optics'
        self.tempdir=None
        self.elepath=self.root+'/ElegantRun'
        self.order = []
        self.magnet= []
        self.undulator = []
        self.RF = []
        self.Energy = 140
        self.EProfile = []
        self.timer = QTimer()
        self.timer.timeout.connect(self.progress)
        self.betax0=30.
        self.betay0=30.
        self.alphax0=0.
        self.alphay0=0.

    def getSettings(self):
        settings={}
        for ele in self.Facility.ElementDB.keys():
            comp = self.Facility.ElementDB[ele]
            if '.MQUA' in ele or '.MQSK' in ele:
                name='%s:K1L' % ele.replace('.','-')
                settings[name]=comp.Length*comp.k1
            if '.MSEX' in ele:
                name='%s:K2L' % ele.replace('.','-')
                settings[name]=comp.Length*comp.k2
            if '.MBND' in ele:    
                name='%s:K0L' % ele.replace('.','-')
                settings[name]=float(comp.angle)
            if '.RACC' in ele:
                name = ele.replace('.','-')
                settings['%s:Grad' % name] = comp.Gradient
                settings['%s:Phase' % name] = float(comp.Phase)
            if '.UDCP' in ele:
                name = ele.replace('.','-')
                settings['%s:Gap' % name] = float(comp.gap)
                settings['%s:Offset'% name] = float(comp.offset)
            if '.UIND' in ele:
                name = ele.replace('.','-')
                settings['%s:K' % name] = float(comp.K)
                settings['%s:kx'% name] = float(comp.kx)
                settings['%s:ky'% name] = float(comp.ky)  
            if '.MKAC' in ele or '.MKDC' in ele:
                name = '%s:KICK' % ele.replace('.','-')
                settings[name]=comp.design_kick
        return settings

    def setSettings(self,settings):
        for setting in settings.keys():
            names=setting.split(':')
            PV=names[0].replace('-','.')
            if PV in self.Facility.ElementDB.keys():
                ele = self.Facility.ElementDB[PV]
                if '.MQUA' in PV or '.MQSK' in PV:                
                    ele.k1=settings[setting]/ele.Length
                if '.MSEX' in PV:
                    ele.k2=settings[setting]/ele.Length
                if '.MBND' in PV:
                    ele.angle = settings[setting]
                if '.RACC' in PV:
                    if 'Grad' in names[1]:
                        ele.Gradient=settings[setting]
                    if 'Phase' in names[1]:
                        ele.Phase = settings[setting]
                if '.UDCP' in PV:
                    if 'Gap' in names[1]:
                        ele.gap=settings[setting]
                    if 'Offset' in names[1]:
                        ele.offset = settings[setting]
                if '.UIND' in PV:
                    if 'K' in names[1]:
                        ele.K=settings[setting]
                    if 'kx' in names[1]:
                        ele.kx = settings[setting]
                    if 'ky' in names[1]:
                        ele.ky = settings[setting]
                if '.MKAC' in PV or '.MKDC' in PV:
                    ele.design_kick=settings[setting]


    def setLine(self, destination):
        #   selecting destination and the model
        self.dest = 'S10BD01'
        if destination == 1:
            self.dest = 'SARBD02'
        elif destination == 2:
            self.dest = 'SATBD02'
        elif destination == 3:
            self.dest = 'SPOSY02'
        sec = self.Facility.getSection(self.dest)
        path = sec.mapping
        self.line = self.Facility.BeamPath(path)
        if 'SPOP301' in self.Facility.SectionDB.keys():
            self.line.setRange('SINLH01','SPOP301')
        else:
            self.line.setRange('SINLH01')

    def progress(self):
        if not self.p.poll() is None:
            self.timer.stop()
            self.trackingDone.emit(1)
            return
        idx = 100
        while True:
            line = self.p.stdout.readline()
            if not line or idx is 0:
                break
            if 'entr' in line or 'call' in line or 'info' in line or 'lot' in line or 'flattened' in line:
                idx -= 1
            else:
                self.Log.appendPlainText(line.strip())
                idx -= 1
        if idx is 0:
            self.timer.start(10)



    def matchLattice(self, variables, scripts, scriptpath):
        if self.tempdir is not None:
            self.tempdir.cleanup()
            self.tempdir=None
        self.tempdir=tempfile.TemporaryDirectory()
        print(self.tempdir)

        self.writeLattice()
        for var in variables.keys():
            self.Madx.write('%s := %s;\n' % (var, variables[var]))
        self.Madx.write('use, sequence=swissfel;\n')
        for ele in scripts:
            self.Madx.write('! Imported Matching Script: %s\n' % ele)
            with open('%s/%s' % (scriptpath,ele), 'r') as f:
                for line in f:
                    self.Madx.write(line)

        self.Madx.write('select, flag = save, clear;\n')
        self.Madx.write('select,flag = save, class = variable, pattern="k1";\n')
        self.Madx.write('save, file = "%s/SetVal_Quad.dat";\n' % self.tempdir.name)

        self.Madx.write('select, flag = save, clear;\n')
        self.Madx.write('select,flag = save, class =variable, pattern="msex.*k2";\n')
        self.Madx.write('save, file = "%s/SetVal_Sext.dat";\n' % self.tempdir.name)

        self.Madx.write('select, flag = save, clear;\n')
        self.Madx.write('select, flag = save, class =variable, pattern="s[23].*mk.*c1";\n')
        self.Madx.write('save, file = "%s/SetVal_Kicker.dat";\n' % self.tempdir.name)

        self.Madx.write('select, flag = save, clear;\n')
        self.Madx.write('select, flag = save, class =variable, pattern="matching";\n')
        self.Madx.write('save, file = "%s/Matching.dat";\n' % self.tempdir.name)
        self.writeLatticeTracking()


        with open(self.tempdir.name + '/tmp-lattice.madx', 'w') as f:
            for line in self.Madx.cc:
                f.write(line)
        self.Log.appendPlainText('Matching with MadX...')
        chdir(self.tempdir.name)
        self.p = Popen(["/sf/bd/bin/madx", "tmp-lattice.madx"], stdout=PIPE, bufsize=1,
                       universal_newlines=True)
        self.timer.start(10)

    def trackLattice(self,variables):
        self.writeLattice()
        for var in variables.keys():
            self.Madx.write('%s := %s;\n' % (var, variables[var]))
        self.writeLatticeTracking()
        if self.tempdir is not None:
            self.tempdir.cleanup()
            self.tempdir=None
        self.tempdir=tempfile.TemporaryDirectory()
        print(self.tempdir)
        with open(self.tempdir.name + '/tmp-lattice.madx', 'w') as f:
            for line in self.Madx.cc:
                f.write(line)
        self.Log.appendPlainText('Tracking with MadX...')
        chdir(self.tempdir.name)
        self.p = Popen(["/sf/bd/bin/madx", "tmp-lattice.madx"], stdout=PIPE, bufsize=100,
                       universal_newlines=True)
        self.timer.start(10)

    def exportLattice(self, filename):
        self.writeLattice()
        self.writeLatticeTracking()
        print('*** Filename:',filename)
        with open(filename, 'w') as f:
            for line in self.Madx.cc:
                f.write(line)

    def writeLattice(self,initopt=None):            
        self.Madx.clear()
        self.Madx.write('option,-echo;\n')
#        self.Madx.write('betax0=30;\n')
#        self.Madx.write('betay0=30;\n')
#        self.Madx.write('alphax0=0;\n')
#        self.Madx.write('alphay0=0;\n\n')
        ax = 3.64
        bx = 12.8 +0.24*ax
        ay = 5.34
        by = 13.6+0.24*ay

        self.Madx.write('betax0=%f;\n' % bx)
        self.Madx.write('betay0=%f;\n' % by)
        self.Madx.write('alphax0=%f;\n' % ax)
        self.Madx.write('alphay0=%f;\n\n' % ay)
        self.Madx.write('beam, particle=electron,energy=140,sigt=1e-3,sige=1e-4;\n\n')

        # update fixed angle of bending magnets for the different paths
        if 'SAT' in self.dest:
            ang = self.Facility.getRegExpElement('S20SY02', 'MKAC', 'design_kick')
            self.Facility.setRegExpElement('S20SY02', 'MKAC0.0', 'cory', ang[0])
            ang = self.Facility.getRegExpElement('S20SY02', 'MKDC', 'design_kick')
            self.Facility.setRegExpElement('S20SY02', 'MKDC0.0', 'cory', ang[0])
            self.Facility.setRegExpElement('S20SY02', 'MBND', 'angle', 2)
        else:
            self.Facility.setRegExpElement('S20SY02', 'MK.C0.0', 'cory', 0)
            self.Facility.setRegExpElement('S20SY02', 'MBND', 'angle', 0)

        if 'SPO' in self.dest:
            ang = self.Facility.getRegExpElement('S30CB15', 'MKAC', 'design_kick')
            if len(ang)> 0:
                self.Facility.setRegExpElement('S30CB15', 'MKAC0.0', 'cory', ang[0])
                ang = self.Facility.getRegExpElement('S30CB15', 'MKDC', 'design_kick')
                self.Facility.setRegExpElement('S30CB15', 'MKDC0.0', 'cory', ang[0])
            self.Facility.setRegExpElement('S30SY01', 'MBND', 'angle', -2.55)
        else:
            self.Facility.setRegExpElement('S30CB15', 'MK.C0.0', 'cory', 0)
            self.Facility.setRegExpElement('S30SP01', 'MBND', 'angle', 0)


        if 'S10' in self.dest:
            self.Facility.setRegExpElement('S10DI01', 'MBND', 'angle', 20)
        else:
            self.Facility.setRegExpElement('S10DI01', 'MBND', 'angle', 0)

        # write the lattice
        self.line.writeLattice(self.Madx, self.Facility)  # write lattice to madx

    def writeLatticeTracking(self):
        self.Madx.write('use, sequence=swissfel;\n')
        if not 'SPOP301' in self.Facility.SectionDB.keys():
            self.Madx.write('Select,flag=Error,pattern="S[AP][TO].*";\n')
        else:
            self.Madx.write('Select,flag=Error,pattern="SAT.*";\n')
        self.Madx.write('EALIGN,DY=0.01;\n')
        self.Madx.write('select, flag=twiss, column=NAME,S,BETX,ALFX,BETY,ALFY,X,PX,Y,PY,DX,DPX,DDX,DY,DPY,DDY,MUX,MUY,RE56;\n')
        self.Madx.write(
            'twiss, range=#s/#e,rmatrix, sequence=swissfel,betx=betax0,bety=betay0,alfx=alphax0,alfy=alphay0,''file="twiss.dat";\n')
        self.Madx.write('plot, haxis = s, vaxis = betx, bety, range =  #s/#e,colour=100;\n')
        self.Madx.write('plot, haxis = s, vaxis = dx, dy, range =  #s/#e,colour=100;\n')
        self.Madx.write('exit;')

    def convertPS2PNG(self):

        if self.tempdir is None:
            return 0
        for f in listdir(self.tempdir.name):
            if '.png' in f:
                remove(join(self.tempdir.name, f))
        run(['gs', '-sDEVICE=png16m', '-o',self.tempdir.name+'/madx%d.png', self.tempdir.name+'/madx.ps', '-c', 'quit'])
        onlyfiles = [f for f in listdir(self.tempdir.name) if isfile(join(self.tempdir.name, f))]
        idx = 0
        for ele in onlyfiles:
            if 'mad' in ele and '.png' in ele:
                idx += 1
        print('PNG parsing done')
        return idx

    def parseTwissFile(self):
        start = False
        skipline = False
        res = []
        with open(self.tempdir.name+'/twiss.dat') as f:
            for line in f:
                if '* NAME' in line:
                    names = line.split()[1:]
                    res.append(names)
                    start = True
                    skipline = True
                    continue
                elif start is False:
                    continue
                elif skipline is True:
                    skipline = False
                    continue
                else:
                    val = line.split()
                    res.append(val)

        return res

    def parseMatchOutputFile(self, mag, name,Parse=False):
        try:
            fin=open('%s/SetVal_%s.dat' % (self.tempdir.name, name), 'r')
        except FileNotFoundError:
            return mag

        for line in fin:
            ele = line.split('=')
            tag = ele[0][0:15].replace('.', '-').upper()
            val = float(ele[1][0:-2])
            OMele=self.Facility.getElement(ele[0][0:15])
            if Parse:
                print(tag,val)
            L=1
            if OMele is not None:
                if 'k2' in OMele.__dict__.keys() or 'k1' in OMele.__dict__.keys():
                    L=OMele.Length 
            mag[tag] = '%f' % (val*L)
        fin.close()
        return mag

    def parseMatchedFile(self):
        mag = {}
        mag = self.parseMatchOutputFile(mag, 'Quad')
        mag = self.parseMatchOutputFile(mag, 'Sext')
        mag = self.parseMatchOutputFile(mag, 'Kicker')
        self.updateMagModel(mag)

        try:
            fin=open('%s/Matching.dat' % (self.tempdir.name), 'r')
        except FileNotFoundError:
            return mag
        self.Log.appendPlainText(' ')
        self.Log.appendPlainText('Matching Quality:')

        for line in fin:
            self.Log.appendPlainText(line.strip())
        fin.close()
        return mag


    # ---------------
    #  event handler

    def writeQuadrupole(self, ele):
        if 'MQUP' in ele.Name:
            return
        nam=ele.Name.replace('.','-')
        self.order.append(nam)
        self.magnet.append({'Name':nam,'kL':ele.k1*ele.Length})
        return

    def writeSextupole(self, ele):
        nam=ele.Name.replace('.','-')
        self.order.append(nam)
        self.magnet.append({'Name':nam,'kL':ele.k2*ele.Length})
        return

    def writeCorrector(self, ele):
        self.order.append(ele.Name.replace('.', '-'))
        return

    def writeUndulator(self, ele):
        if 'UDCP' in ele.Name:
            self.undulator.append({'Name': ele.Name.replace('.','-'), 'K': [ele.gap,float(ele.offset)]})
        if 'UMOD' in ele.Name:
            self.undulator.append({'Name': ele.Name.replace('.', '-'), 'K': [ele.K]})
        if 'UIND' in ele.Name:
            if 'SATUN' in ele.Name:
                if ele.ky>0.6:
                    pol=0
                elif ele.kx > 0.6:
                    pol=1
                else:
                    pol=3
                self.undulator.append({'Name': ele.Name.replace('.', '-'), 'K': [ele.K,pol]})

            else:
                self.undulator.append({'Name': ele.Name.replace('.', '-'), 'K': [ele.K]})


    def writeRF(self, ele):
        if 'RACC' in ele.Name:
            idx = int(ele.Name[12:13])
            if idx is 1:
                self.RF.append({'Name': ele.Name[0:7], 'Grad': ele.Gradient * 1e-6 * ele.Length, 'Phase': ele.Phase})
            else:
                self.RF[-1]['Grad'] += ele.Gradient * 1e-6 * ele.Length
            self.Energy += ele.Gradient * 1e-6 * ele.Length * np.sin(ele.Phase / 180. * np.pi)

    def writeBend(self, ele):
        nam=ele.Name.replace('.','-')

        if 'SINLH02.MBND100' in ele.Name:
            self.order.append(nam)
            self.magnet.append({'Name':nam,'kL':np.abs(ele.angle)})
        if 'SINBC02.MBND100' in ele.Name:
            self.order.append(nam)
            self.magnet.append({'Name':nam,'kL':np.abs(ele.angle)})
            self.EProfile.append({'Name': 'BC1', 'Energy': self.Energy})
        if 'S10BC02.MBND100' in ele.Name:
            self.order.append(nam)
            self.magnet.append({'Name':nam,'kL':np.abs(ele.angle)})
            self.EProfile.append({'Name': 'BC2', 'Energy': self.Energy})
        if 'S20SY02.MBND200' in ele.Name:
#            self.order.append(nam)
#            self.magnet.append({'Name':nam,'kL':np.abs(ele.angle)})
            self.EProfile.append({'Name': 'Switch Yard', 'Energy': self.Energy})
        if 'SARCL02.MBND100' in ele.Name:
            self.order.append(nam)
            self.magnet.append({'Name':nam,'kL':np.abs(ele.angle)})
            self.EProfile.append({'Name': 'Aramis', 'Energy': self.Energy})
        if 'SATMA01.MBND300' in ele.Name:
            self.order.append(nam)
            self.magnet.append({'Name':nam,'kL':np.abs(ele.angle)})
            self.EProfile.append({'Name': 'Athos', 'Energy': self.Energy})
        if 'SATUN05.MBND100' in ele.Name or 'SATUN14.MBND100' in ele.Name:
            self.order.append(nam)
            self.magnet.append({'Name':nam,'kL':np.abs(ele.angle)})
        if 'S10' in self.dest and 'S10DI01.MBND100' in ele.Name:
            self.EProfile.append({'Name': 'Injector', 'Energy': self.Energy})

    # basic online model interface

    def getModelInfo(self,erg=140): # calculate the energy profile
        self.order.clear()
        self.magnet.clear()
        self.undulator.clear()
        self.RF.clear()
        self.EProfile.clear()
        self.Energy = erg
        self.EProfile.append({'Name': 'Laser Heater', 'Energy': self.Energy})
        self.line.writeLattice(self, self.Facility)

    def updateUndModel(self, und):
        for name in und.keys():
            val = und[name]
            ele = self.Facility.getElement(name.replace('-', '.'))
            if 'UDCP' in name:
                ele.gap=val['K']
                ele.offet=val['Pol']
                continue
            ele.K = val['K']
            if 'SATUN' in name:
                if 'LH' in val['Pol']:
                    ele.kx=0
                    ele.ky=1
                elif 'LV' in val['Pol']:
                    ele.kx=1
                    ele.ky=0
                else:
                    ele.kx=0.5
                    ele.ky=0.5


    def scaleRF(self, dEref, rf, stations):
        dE = 0
        for stat in stations:
            if stat in rf.keys():
                dE += rf[stat]['Grad'] * np.sin(rf[stat]['Phase'] * np.pi / 180)
        if np.abs(dE) < 1e-6:
            scl = 1.
        else:
            scl = dEref / dE
        print('Scaling', scl)
        for stat in stations:
            if stat in rf.keys():
                rf[stat]['Grad'] *= scl
                if rf[stat]['Grad'] < 0:
                    rf[stat]['Grad'] *= -1.
                    rf[stat]['Phase'] += 180.
        return rf

    def updateRFModel(self, rf, erg=None,Einit=140):
        if erg is not None:
            e0 = Einit
            if 'BC1' in erg.keys():
                rf = self.scaleRF(erg['BC1'] - e0, rf, ['SINSB03', 'SINSB04', 'SINXB01'])
                e0 = erg['BC1']
            if 'Injector' in erg.keys():
                rf = self.scaleRF(erg['Injector'] - e0, rf, ['S10CB%2.2d' % (i+1) for i in range(2)])
                e0 = erg['Injector']
            if 'BC2' in erg.keys():
                rf = self.scaleRF(erg['BC2'] - e0, rf, ['S10CB%2.2d' % (i+1) for i in range(9)])
                e0 = erg['BC2']
            if 'Switch Yard' in erg.keys():
                rf = self.scaleRF(erg['Switch Yard'] - e0, rf, ['S20CB%2.2d' % (i+1) for i in range(4)])
                e0 = erg['Switch Yard']
            if 'Aramis' in erg.keys():
                rf = self.scaleRF(erg['Aramis'] - e0, rf, ['S30CB%2.2d' % (i+1) for i in range(13)])
                e0 = erg['Aramis']
            if 'Athos' in erg.keys():
                rf = self.scaleRF(erg['Athos'] - e0, rf, ['SATCB01'])
                e0 = erg['Athos']
        for ele in rf.keys():
            listRF = self.Facility.listElement(ele+'.RACC',1)
            phase = rf[ele]['Phase']
            grad  = rf[ele]['Grad']/len(listRF)*1e6
#            print(ele,phase,grad,len(listRF))
            for acc in listRF:
                acc.Phase = phase
                acc.Gradient = grad/acc.Length
        self.Facility.forceEnergyAt('SINLH01.DBAM010',Einit*1e6)


        
    def updateMagModel(self, mag):
        for name in mag.keys():
            ele = self.Facility.getElement(name.replace('-', '.'))
            if ele is None:
                self.Log.appendPlainText('*** Warning: Updating Model')
                self.Log.appendPlainText('*** The element %s cannot be found in the online model' % name)
            else:
                if 'k2' in ele.__dict__.keys():
                    ele.k2 = float(mag[name])/ele.Length
                elif 'k1' in ele.__dict__.keys():
                    ele.k1 = float(mag[name])/ele.Length
                elif 'design_kick' in ele.__dict__.keys():
                    ele.design_kick = float(mag[name])
                elif 'design_angle' in ele.__dict__.keys():
#                    print('update Magnet angle for',ele.Name,mag[name])
                    self.Facility.setRegExpElement(ele.Name[0:7],'MBND','angle',float(mag[name]))
                else:
                    self.Log.appendPlainText('*** Warning: Updating Model')
                    self.Log.appendPlainText('*** The element %s is neither Quadrupole, Sextupole or Kicker' % name)


    def writeMagnets(self,magnets,Log=None):
        pvs={}
        for mag in magnets.keys():
            if 'MQUA' in mag:
                pvs['%s:K1L-SET' % mag]=magnets[mag]
            if 'MSEX' in mag:
                pvs['%s:K2L-SET' % mag]=magnets[mag]

        if not Log is None:
            for key in pvs.keys():
                Log.appendPlainText('%s : %f' % (key,pvs[key]))
        self.MI.writePVs(pvs)

    def importElegantDistribution(self,distinfile):
        distoutfile = "SwissFEL_dist.sdds"
        system('ssh sf-lc7.psi.ch "cd %s && bash runImportDist.sh %s"' % (self.elepath,distinfile))
        if self.dist is None:
            self.dist = Distribution.Dist()
        self.dist.importSDDS(distoutfile, self.elepath, 1)

    def exportElegantLattice(self, filename,dump=[],DS=1,LSC=False,CSR=False,center=[],P0=None):
        ele = Elegant.Elegant()
        ele.distsample=DS
        ele.p0=P0
        if LSC:
            ele.lsc=1
        else:
            ele.lsc=0
        if CSR:
            ele.csr=1
        else:
            ele.csr=0
        ele.dump=dump
        ele.center=center
        if self.dist is None:
            print('*** Warning: Distribution not defined. Only Lattice will be written')
            ele.openLatticeStream(self.elepath, filename[0:-4])
            self.line.writeLattice(ele, self.Facility)
            ele.closeLatticeStream()
        else:
            ele.charge=self.dist.q
            ele.openStream(filename[0:-4], self.elepath, self.dist)
            self.line.writeLattice(ele, self.Facility)
            ele.closeStream()

    def trackElegant(self):
        if self.dist is None:
            return
        self.exportElegantLattice('SwissFEL.ele')
        self.Log.appendPlainText('Tracking with Elegant..')
        
        system('ssh sf-lc7.psi.ch "cd %s && bash runElegant.sh SwissFEL"' % (self.elepath))
        self.p=Popen(['ls'], stdout=PIPE, bufsize=100, universal_newlines=True)
        self.timer.start(10)

    def convertElegant2PNG(self):

# the conversion is done with the script run Elegant.sh in the working directory

#        for f in listdir('.'):
#            if '.png' in f:
#                remove(join('.', f))
#        f = open('madx1.png', 'w')
#        run(['sddsplot', 'SwissFEL.twi', '-col=s,beta[xy]', '-device=png'], stdout=f)
#        f.close()
#        f = open('madx2.png', 'w')
#        run(['sddsplot', 'SwissFEL.twi', '-col=s,pCentral0', '-graph=line,vary', '-device=png'], stdout=f)
#        f.close()
        return 3
