import sys
import re
import numpy as np
import copy
import os
import json
from datetime import datetime
import time
from shutil import copyfile
from os.path import isfile, join

from PyQt5 import QtWidgets,QtGui
from PyQt5.uic import loadUiType
from PyQt5.QtGui import QPixmap, QTransform
from PyQt5.QtWidgets import QMessageBox

from Model import *
from MadXTracker import *
from OpticsPlotting import *

# import local modules
from epics import caput

Ui_OpticsGUI, QMainWindow = loadUiType('Optics.ui')


class Optics(QMainWindow, Ui_OpticsGUI):
    def __init__(self,arg=0):
        super(Optics, self).__init__()
        self.setupUi(self)

        self.version='1.4.2'
        self.setWindowIcon(QtGui.QIcon("rsc/iconoptics.png"))
        self.setWindowTitle("SwissFEL Optics Tracking and Matching")
        self.homedir=os.getcwd()

        self.plot=OpticsPlotting()
        self.plot.initmpl(self.mplvl,self.mplwindow)

        self.isMachine=True
        if arg > 0:
            self.isMachine =False
        self.madx=MadXTracker(self.Log,arg)

        # program variables
        self.busy = 0
        self.png=1
        self.pngmax=1
        self.dorotate = True

        # all events to handle model and scripts
        self.modelmanager = Model()
        sorder=[self.UIScriptOrder1,self.UIScriptOrder2,self.UIScriptOrder3,self.UIScriptOrder4]
        vorder=self.UIScriptVar
        self.modelmanager.init(sorder,vorder,self.ScriptList,self.UIModelName,self.UIModelDesc)
        self.modelmanager.initScripts(self.Editor,self.EditorFile)
        self.UIScript1Up.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript1Down.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript1Add.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript1Remove.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript2Up.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript2Down.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript2Add.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript2Remove.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript3Up.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript3Down.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript3Add.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript3Remove.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript4Up.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript4Down.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript4Add.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIScript4Remove.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIVarAdd.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))
        self.UIVarRemove.clicked.connect(lambda: self.modelmanager.eventScript(self.sender().objectName()))

        self.actionSave_Model.triggered.connect(self.saveModel)
        self.actionSave_Model_as.triggered.connect(self.saveModel)
        self.actionOpen.triggered.connect(self.loadModel)
        self.actionNew.triggered.connect(self.modelmanager.new)
        self.actionBackup.triggered.connect(self.backup)

        # all events of matching and tracking
        self.actionMatch.triggered.connect(self.matchModel)
        self.actionTrack.triggered.connect(self.matchModel)
        self.UIMatch.clicked.connect(self.matchModel)
        self.UITrack.clicked.connect(self.trackModel)
        self.madx.trackingDone.connect(self.trackingDone)   # signal when matching is done
        self.UIDest.currentIndexChanged.connect(self.changeDestination)

        #all action for optics plotting
        self.PBetax.toggled.connect(self.doplot)
        self.PAlphax.toggled.connect(self.doplot)
        self.PBetay.toggled.connect(self.doplot)
        self.PAlphay.toggled.connect(self.doplot)
        self.PEtax.toggled.connect(self.doplot)
        self.PEtay.toggled.connect(self.doplot)
        self.PR56.toggled.connect(self.doplot)
        self.PEnergy.toggled.connect(self.doplot)
        self.PStart.editingFinished.connect(self.doplot)
        self.PEnd.editingFinished.connect(self.doplot)

        # Help
        self.actionInfo.triggered.connect(self.about)
        
        # actions for madx plotting
        self.NextPNG.clicked.connect(self.browsePNG)
        self.PrevPNG.clicked.connect(self.browsePNG)
        self.PagePNG.editingFinished.connect(self.browsePNG)

        # all actions for scripts
        self.actionOpen_Script.triggered.connect(self.openScript)
        self.actionNew_Script.triggered.connect(self.modelmanager.scripts.newScript)
        self.actionSave_Script.triggered.connect(self.saveScript)
        self.actionDuplicate_Script.triggered.connect(self.saveAsScript)
        self.ScriptList.itemDoubleClicked.connect(self.openSelectedScript)
        self.Editor.textChanged.connect(self.modelmanager.scripts.edited)

        # sandbox functionality
        self.actionRefreshSettings.triggered.connect(self.refresh)
        self.actionLoad_From_LongTracker.triggered.connect(self.updateSettingsFromLongTracker)
        self.actionExport_Optics_Functions.triggered.connect(self.exportOpticsTable)
        self.MagReStart.editingFinished.connect(self.refresh)
        self.MagReFilter.editingFinished.connect(self.refresh)
        self.Mach2ModMag.clicked.connect(self.Mach2Mod)
        self.Mach2ModRF.clicked.connect(self.Mach2Mod)
        self.Mach2ModUnd.clicked.connect(self.Mach2Mod)
        self.SB2ModMag.clicked.connect(self.Sandbox2Mod)
        self.SB2ModRF.clicked.connect(self.Sandbox2Mod)
        self.SB2ModUnd.clicked.connect(self.Sandbox2Mod)
        self.SB2MachineMag.clicked.connect(self.writeMagToMachine)
        self.refresh()

        # action for Elegant and MadX

        self.EleDumpRemove.clicked.connect(self.RemoveEleDump)
        self.EleDumpAdd.clicked.connect(self.AddEleDump)
        self.actionExport_Lattice.triggered.connect(self.exportLattice)
        self.actionExport_Lattice_for_Elegant.triggered.connect(self.exportLattice)
        self.actionImport_Initial_Distribution.triggered.connect(self.importDistribution)

        print('Init done...')

#----------------------------------
# event handler for Model/Script Actions

    def saveModel(self):
        if self.modelmanager.saveName is None or '_as' in self.sender().objectName():
            options = QtWidgets.QFileDialog.Options()
            options |= QtWidgets.QFileDialog.DontUseNativeDialog
            fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save Model",
                                                            self.modelmanager.path + '/newModel.json',
                                                            "Json Files (*.json)", options=options)
            if not fileName:
                return
            fname=fileName.split('/')[-1]
        else:
            fname=self.modelmanager.saveName
        self.modelmanager.settings=self.madx.getSettings()
        self.modelmanager.save(fname)
        
    def loadModel(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open Model",
                                                            self.modelmanager.path,
                                                            "Json Files (*.json)", options=options)
        if not fileName:
            return
        fileName = fileName.split('/')[-1]
        self.modelmanager.load(fileName)
        self.Matched1.setChecked(False)
        self.Matched2.setChecked(False)
        self.Matched3.setChecked(False)
        self.Matched4.setChecked(False)
        self.madx.setSettings(self.modelmanager.settings)

 

#----------------------------------------------------
# matching and tracking
    def changeDestination(self):
        dest=self.UIDest.currentIndex()
        self.madx.setLine(dest)
        self.refresh()    # update settings panel

    def trackModel(self):
        if not self.busy is 0:
            return
        dest=self.UIDest.currentIndex()
        self.Status.setText('Tracking...')
        self.busy = -1  # negative = tracking
        self.Log.clear()
        self.Log.appendPlainText('Writing Madx Lattice...')
        self.madx.setLine(dest)
        self.madx.trackLattice(self.modelmanager.variables)

    def matchModel(self):
        if not self.busy is 0:
            return
        self.Status.setText('Matching...')
        dest=self.UIDest.currentIndex()
        var = self.modelmanager.variables
        if (self.UIInitQuads.isChecked()):
            var['initquads']=1
        else:
            var['initquads']=0
        self.busy = 1  # positive = matching
        
        if self.Matched1.isChecked():
            scripts=[]
        else:
            scripts=self.modelmanager.matchorder['Common']
        self.madx.setLine(dest)

        if dest is 1:
            scripts+=self.modelmanager.matchorder['Aramis']
        elif dest is 2:
            scripts+=self.modelmanager.matchorder['Athos']
        elif dest is 3:
            scripts+=self.modelmanager.matchorder['Porthos']

        self.Log.clear()
        self.Log.appendPlainText('Writing Madx Lattice...')
        self.madx.matchLattice(var,scripts,self.modelmanager.scripts.path)

    def trackingDone(self,status):
        
        if self.busy > 0:  # matching and not tracking
            mag=self.madx.parseMatchedFile()  # the model is actually updated within madx.
            if len(mag.keys()) < 1:
                self.busy=0
                self.Status.setText('Matching failed')
                return
            self.Matched1.setChecked(True)
            dest=self.UIDest.currentIndex()
            if dest == 1:
                self.Matched2.setChecked(True)
            elif dest == 2:
                self.Matched3.setChecked(True)
            elif dest == 4:
                self.Matched4.setChecked(True)
        self.refresh()
        self.updateOpticsTable()
        self.saveOpticsinModel()
        self.initPNG()
        self.Status.setText('Idle')
        self.busy = 0

    def saveOpticsinModel(self):
        nrow=self.OpticsList.rowCount()
        if nrow < 1:
            return
        data={}
        fields=['S', 'BETAX','ALPHAX','BETAY','ALPHAY','X','PX','Y','PY','ETAX','ETAPX','ETAX2','ETAY','ETAPY','ETAY2','MUX','MUY']
        for icol in range(1,len(fields)+1):
            val=[]
            for irow in range(nrow):
                val.append(float(str(self.OpticsList.item(irow,icol).text())))
            data[fields[icol-1]]=val
        line='Aramis'
        if 'SAT' in self.madx.dest:
            line='Athos'
        self.modelmanager.optics[line]=data


    def backup(self):
        path = self.modelmanager.path
        for f in listdir(path):
            fp=join(path,f)
            if isfile(fp):
                ft=join(self.homedir+'/SavedOpticsModels',f)
                copyfile(fp,ft)
        path = self.modelmanager.scripts.path
        for f in listdir(path):
            fp=join(path,f)
            if isfile(fp):
                ft=join(self.homedir+'/SaveOpticsScripts',f)
                copyfile(fp,ft)

        
    #-------------
    # Plotting events

    def doplot(self):
        ncol=self.OpticsList.columnCount()
        nrow=self.OpticsList.rowCount()
        if nrow <2:
            print('No Optics defined')
            return

        zstart=float(str(self.PStart.text()))
        zend=float(str(self.PEnd.text()))
        data={}
        for icol in range(ncol):
            dat=[]
            label=str(self.OpticsList.horizontalHeaderItem(icol).text())
            if icol == 0:
                for irow in range(nrow):
                    dat.append(str(self.OpticsList.item(irow,icol).text()))    
            else:
                for irow in range(nrow):
                    dat.append(float(str(self.OpticsList.item(irow,icol).text())))    
            data[label]=dat           
        filt={}
        filt['BETX']=self.PBetax.isChecked()
        filt['BETY']=self.PBetay.isChecked()
        filt['ALFX']=self.PAlphax.isChecked()
        filt['ALFY']=self.PAlphay.isChecked()
        filt['DX']=self.PEtax.isChecked()
        filt['DY']=self.PEtay.isChecked()
        filt['RE56']=self.PR56.isChecked()
        filt['Energy']=self.PEnergy.isChecked()
        self.plot.plot(data,filt,zstart,zend)


    def about(self):
        QMessageBox.about(self, 'About Optics', "SwissFEL Optics\nwritten by Sven Reiche\nVersion : %s" % self.version)


    #------------------------------------
    # display the madx.ps output as PNG

    def initPNG(self):
        if self.busy is 1 or self.busy is -1:
            self.pngmax=self.madx.convertPS2PNG()
            self.dorotate=True
        else:
            self.pngmax=self.madx.convertElegant2PNG()
            self.dorotate=False
        self.png=1
        self.LabelPNG.setText('of %d' % self.pngmax)
        self.PagePNG.setText('%d' % self.png)
        self.showPNG()

    def showPNG(self):
        if self.pngmax < 1:
            return
        filename = '%s/madx%d.png' % (self.madx.tempdir.name,self.png)
        if self.dorotate:
            self.PlotPNG.setPixmap(QPixmap(filename).transformed(QTransform().rotate(90)))
        else:
            self.PlotPNG.setPixmap(QPixmap(filename))

    def browsePNG(self):
        name=self.sender().objectName()
        if 'Prev' in name:
            self.png=self.png-1
            if self.png<1:
                self.png=1
        if 'Next' in name:
            self.png=self.png+1
            if self.png>self.pngmax:
                self.png=self.pngmax
        if 'Page' in name:
            idx=int(str(self.PagePNG.text()))
            if idx > self.pngmax:
                self.png=self.pngmax
            elif idx < 1:
                self.png=1
            else:
                self.png=idx
        self.PagePNG.setText('%d' % self.png)
        self.showPNG()
  
    # -----------------------------
    # Update Script List

    def openSelectedScript(self):
        file = str(self.ScriptList.currentItem().text())
        self.modelmanager.scripts.openScript(file)

    def openScript(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open Script",self.modelmanager.scripts.path, 
                                                            "MadX Scripts (*.madx)", options=options)
        if fileName:
            fileName = fileName.split('/')[-1]
            self.modelmanager.scripts.openScript(fileName)

    def saveScript(self):
        if self.modelmanager.scripts.hasFile:
            self.modelmanager.scripts.saveScript()
        else:
            self.saveAsScript()

    def saveAsScript(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save Script",
                                                            self.modelmanager.scripts.path + '/'+str(self.EditorFile.text()),
                                                            "MadX Scripts (*.madx)", options=options)
        if not fileName:
            return
        fileName = fileName.split('/')[-1]
        self.modelmanager.scripts.saveScript(fileName)
        self.modelmanager.displayScripts()
 

    #--------------------------------------------------
    # optics table events



    def exportOpticsTable(self):
        ncol=self.OpticsList.columnCount()
        nrow=self.OpticsList.rowCount()
        if nrow <2:
            return
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save Model",
                                                            'optics.json',
                                                            "Json Files (*.json)", options=options)
        if not fileName:
            return
        data={}
        for icol in range(ncol):
            dat=[]
            label=str(self.OpticsList.horizontalHeaderItem(icol).text())
            if icol == 0:
                for irow in range(nrow):
                    dat.append(str(self.OpticsList.item(irow,icol).text()))    
            else:
                for irow in range(nrow):
                    dat.append(float(str(self.OpticsList.item(irow,icol).text())))    
            data[label]=dat
        
        with open(fileName, 'w') as outfile:
            json.dump(data, outfile,indent=4)


    def updateOpticsTable(self):
        res = self.madx.parseTwissFile()
        self.OpticsList.clear()
        nrow = len(res)
        if nrow is 0:
            return
        ncol = len(res[0])
        self.OpticsList.setColumnCount(ncol+1)
        self.OpticsList.setRowCount(nrow-1)
        val=self.madx.Facility.EnergyAt('SINLH01.DBAM010')[0]
        for i in range(1,nrow):
            for j in range(ncol):
                self.OpticsList.setItem(i-1, j, QtWidgets.QTableWidgetItem(res[i][j]))
            field=res[i][0]
            if len(field) > 14:
                if field[1:16] in self.madx.Facility.EM.Energy.keys():
                    val=np.sum(self.madx.Facility.EnergyAt(field[1:16]))
            self.OpticsList.setItem(i-1, ncol, QtWidgets.QTableWidgetItem('%6.2f' % (val*1e-6)))
        for j in range(ncol):
            self.OpticsList.setHorizontalHeaderItem(j, QtWidgets.QTableWidgetItem(res[0][j]))
        self.OpticsList.setHorizontalHeaderItem(ncol, QtWidgets.QTableWidgetItem('Energy'))
        self.OpticsList.resizeColumnsToContents()
        self.OpticsList.verticalHeader().hide()
        self.OpticsList.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

 

#----------------------------
# setting/sandbox functions
 

    def writeMagToMachine(self):
        if not self.isMachine:
            return
        self.Log.clear()
        self.Log.appendPlainText('Writing filtered magnets value to machine') 
        val=self.getEntrySB(self.MagSB,[1],None)
        self.madx.writeMagnets(val,self.Log)


    def updateSettingsFromLongTracker(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open Settings",
                                                            "/sf/data/applications/BD-LongTrack/Settings",
                                                            "Json Files (*.json)", options=options)
        if not fileName:
            return
        vals={}
        with open(fileName,'r') as infile:
            data=json.load(infile)
            key='SINSB'            
            if key in data.keys():
                for i in range(3,5):
                    vals['SINSB%2.2d' %i]={'Grad':data[key]['Voltage']/2,'Phase':data[key]['Phase']}
            key='SINXB'            
            if key in data.keys():
                for i in range(1,2):
                    vals['SINXB%2.2d' %i]={'Grad':data[key]['Voltage'],'Phase':data[key]['Phase']}
            key='Linac1-1'            
            if key in data.keys():
                for i in range(1,3):
                    vals['S10CB%2.2d' %i]={'Grad':data[key]['Voltage']/2,'Phase':data[key]['Phase']}
            key='Linac1-2'            
            if key in data.keys():
                for i in range(3,10):
                    vals['S10CB%2.2d' %i]={'Grad':data[key]['Voltage']/7,'Phase':data[key]['Phase']}
            key='Linac2'            
            if key in data.keys():
                for i in range(1,5):
                    vals['S20CB%2.2d' %i]={'Grad':data[key]['Voltage']/4,'Phase':data[key]['Phase']}
            key='Linac3'            
            if key in data.keys():
                for i in range(1,14):
                    vals['S30CB%2.2d' %i]={'Grad':data[key]['Voltage']/13,'Phase':data[key]['Phase']}

        Einit=self.madx.Facility.EnergyAt('SINLH01.DBAM010')[0]*1e-6
        self.madx.updateRFModel(vals,None,Einit)
        self.refresh()


#-----------------------------------------------
#  all routine managing the sandbox setting tab


    def getEntrySB(self,sb,col,label):
        value={}
        nrow=sb.rowCount()
        for i in range(nrow):
            name=str(sb.item(i,0).text())
            value[name]={}
            for idx,j in enumerate(col):
                tag=None
                try:
                    tag=float(str(sb.item(i,j).text()))
                except ValueError:
                    tag=str(sb.item(i,j).text())
                if label is None:
                    value[name]=tag
                else:
                    value[name][label[idx]]=tag

        return value

    def copySB(self,sb,source,target):
        nrow=sb.rowCount()
        for i in range(nrow):
            txt=str(sb.item(i,source).text())
            sb.setItem(i,target,QtWidgets.QTableWidgetItem(txt))
    

    def SB2Mod(self,sender):
        if 'Mag' in sender:
            val=self.getEntrySB(self.MagSB,[1],None)
            self.madx.updateMagModel(val)
        elif 'Und' in sender:
            val=self.getEntrySB(self.UndSB,[1,2],['K','Pol'])
            for key1 in val.keys():
                if 'UDCP' in key1:
                    for key2 in val[key1].keys():
                        val[key1][key2]*=1e-3
            self.madx.updateUndModel(val)
        else:
            val=self.getEntrySB(self.RFSB,[1,2],['Grad','Phase'])
            Einit=float(str(self.SBE0Model.text()))
            self.madx.updateRFModel(val,None,Einit)
        self.refresh()

    def Sandbox2Mod(self):
        sender=str(self.sender().objectName())
        self.SB2Mod(sender)

    def Mach2Mod(self):
        sender=str(self.sender().objectName())
        if 'Mag' in sender:
            self.copySB(self.MagSB,2,1)
        elif 'Und' in sender:
            self.copySB(self.UndSB,3,1)
            self.copySB(self.UndSB,4,2)
        else:
            self.SBE0Model.setText(self.SBE0Machine.text())
            self.copySB(self.RFSB,3,1)
            self.copySB(self.RFSB,4,2)
        self.SB2Mod(sender)


    def refresh(self):
 #       return # temporarily disabling machine interface
        self.updateSBMag()
        self.updateSBUnd()
        self.updateSBRF()


    def updateSBRF(self):
        val=self.madx.MI.readEnergy()
        self.SBE0Model.setText('%6.2f' % (self.madx.Facility.EnergyAt('SINLH01.DBAM010')[0]*1e-6))
        if self.isMachine:
            self.SBE0Machine.setText('%6.2f' % (val['SINLH02-MBND100']))
        else:
            self.SBE0Machine.setText('not available')

        labels=['Name',' Model ',' Model ','Machine','Machine']
        self.madx.getModelInfo()   

        val=self.madx.MI.readRF()
        alldata=[]
        for ele in self.madx.RF:
            data=[] 
            data.append(ele['Name'])
            data.append('%6.1f' % ele['Grad'])
            data.append('%7.2f' % ele['Phase'])
            if ele['Name'] in val.keys():
                locval=val[ele['Name']]
                if None in locval:
                    print('Error for',ele['Name'],':',locval)
                    data.append('0.0')
                    data.append('0.0')
                else:
                    data.append('%6.1f' % locval[0])
                    data.append('%7.2f' % locval[1])
            else:
                data.append('0')
                data.append('90')

            alldata.append(data)
        self.updateSandbox(self.RFSB,labels,alldata)

    def updateSBMag(self):
        filt=str(self.MagReStart.text())
        self.found=False
        if filt is '' or filt is '*':
            self.found=True
        regpat=re.compile(filt)
        filt2=str(self.MagReFilter.text())
        filt2='.*'+filt2
        regpat2=re.compile(filt2)

        labels=['Name',' Model ','Machine']
        self.madx.getModelInfo()   
        val=self.madx.MI.readMagnet()
        alldata=[]
        for ele in self.madx.magnet:
            nam=ele['Name']
            if regpat.match(nam) is not None:
                self.found=True
            if not self.found or not regpat2.match(nam):
                continue
            data=[]
            data.append(ele['Name'])
            data.append('%6.3f' % ele['kL'])
            if ele['Name'] in val.keys() and not val[ele['Name']] is None:
                data.append('%6.3f' % val[ele['Name']])
            else:
                data.append('---')
            alldata.append(data)
        self.updateSandbox(self.MagSB,labels,alldata,1)


    def updateSBUnd(self):
        labels=['Name',' Model ',' Model ','Machine','Machine']
        enum=['LH','LV+','LV-','C+','C-','ZL']
        self.madx.getModelInfo()   
        val=self.madx.MI.readID()
        alldata=[]
        for ele in self.madx.undulator:
            data=[] 
            data.append(ele['Name'])
            key=ele['Name']+':K'
            if 'UDCP' in ele['Name']:
                data.append('%5.3f' % (ele['K'][0]*1e3))
                data.append('%5.3f' % (ele['K'][1]*1e3))
                if not self.isMachine:
                    data.append('---')
                    data.append('---')
                elif val[ele['Name']+':K'] is None:   # add some dummy values for non avalable channels                    
                    data.append('%6.3f' % (20.))
                    data.append('%6.3f' % (0.))
                else:
                    data.append('%6.3f' % (val[ele['Name']+':K']))
                    data.append('%6.3f' % (val[ele['Name']+':P']))
            else:
                data.append('%6.4f' % (ele['K'][0]))
                if len(ele['K'])>1:
                    data.append('%f' % (ele['K'][1]))
                else:
                    data.append(enum[0])
                kk=ele['Name']+':K'
                if kk in val.keys() and not val[kk] is None:
                    data.append('%6.4f' % (val[ele['Name']+':K']))
                else:
                    data.append('---')                    
                pol=ele['Name']+':P'
                if pol in val.keys():
                    data.append(enum[val[pol]])
                else:
                    data.append(enum[0])
            alldata.append(data)
        self.updateSandbox(self.UndSB,labels,alldata)


    def updateSandbox(self,sb,labels,alldata,dcol=0):

        sb.clear()
        ncol=len(labels)
        nrow=len(alldata)
        idx=0
        sb.setColumnCount(ncol)
        sb.setRowCount(nrow)
        for i in range(ncol):
            sb.setHorizontalHeaderItem(i, QtWidgets.QTableWidgetItem(labels[i]))
        for i,data in enumerate(alldata):
            for j in range(ncol):
                sb.setItem(i, j, QtWidgets.QTableWidgetItem(data[j]))
                # setting correct color
                if 'Model' in labels[j]:
                    sb.item(i,j).setBackground(QtGui.QColor(250,240,200))
                if 'Machine' in labels[j] and dcol > 0:
                    if '--' in data[j] or '--' in data[j-dcol]:
                        sb.item(i,j).setBackground(QtGui.QColor(100,255,100))
                    else:
                        df=np.abs(float(data[j])-float(data[j-dcol]))
                        if df < 0.001:
                            sb.item(i,j).setBackground(QtGui.QColor(100,255,100))
                        elif df < 0.01:
                            sb.item(i,j).setBackground(QtGui.QColor(255,255,0))
                        else:
                            sb.item(i,j).setBackground(QtGui.QColor(255,100,100))
        sb.resizeColumnsToContents()
        sb.verticalHeader().hide()


   # -------------------------------------
   # Elegant and Madx  Interaction

    def importDistribution(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open Distribution",
                                                            "/sf/bd/simulations/Reference/Input-Distribution",
                                                            "SDDS Files (*.sdds)", options=options)
        if not fileName:
            return
        self.madx.importElegantDistribution(fileName)
        self.EleBetax.setText('%6.2f' % self.madx.dist.betax)
        self.EleBetay.setText('%6.2f' % self.madx.dist.betay)
        self.EleAlphax.setText('%6.2f' % self.madx.dist.alphax)
        self.EleAlphay.setText('%6.2f' % self.madx.dist.alphay)
        self.EleEmitx.setText('%6.2f' % (self.madx.dist.emitx*1e9))
        self.EleEmity.setText('%6.2f' % (self.madx.dist.emity*1e9))
        self.EleCharge.setText('%6.2f' % (self.madx.dist.q*1e12))
        self.EleEnergy.setText('%6.2f' % (self.madx.dist.p0*0.511))
        self.EleSpread.setText('%6.4f' % (self.madx.dist.sige*100))
        self.EleLength.setText('%6.2f' % (self.madx.dist.sigt*1e12))
        self.EleDist.setEnabled(True)
        self.EleParm.setEnabled(True)

    def AddEleDump(self):
        self.EleDumpList.addItem(self.EleNewDump.text())

    def RemoveEleDump(self):
        item=self.EleDumpList.currentRow()
        if item > -1:
            self.EleDumpList.takeItem(item)
 
    def exportLattice(self):
        sender = self.sender()
        isMadx=True
        txt1 = "Save MadX Lattice"
        txt2 = "/Lattice.madx"
        txt3 = "MadX Files (*.madx)"
        path = self.madx.path
        if 'Elegant' in sender.text():
            isMadx = False
            txt1 = "Save Elegant Files"
            txt2 = "/Lattice.ele"
            txt3 = "Elegant Files (*.ele)"
            path = self.madx.elepath
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, txt1, path + txt2, txt3, options=options)
        if not fileName:
            return
        if isMadx:
            self.madx.exportLattice(fileName)
        else:
            fileName = fileName.split('/')[-1]
            LSC=not self.EleLSC.isChecked()
            CSR=not self.EleCSR.isChecked()
            DS=int(self.EleSampleRate.text())
            dump=[]
            center=[]
            if self.EleCenter.isChecked():
                center=['SINDI01.DBPM010','S10MA01.DBPM010','SARMA01.DBPM040','SATSY02.DBPM020', 'SATDI01.DBPM030'] 
            if self.EleDumpDist.isChecked():
                for i in range(self.EleDumpList.count()):
                    dump.append(str(self.EleDumpList.item(i).text()).replace('-','.'))
            self.madx.exportElegantLattice(fileName,dump,DS,LSC,CSR,center)

    # --------------------------------
    # Main routine


if __name__ == '__main__':
    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("plastique"))
    app = QtWidgets.QApplication(sys.argv)
    if len(sys.argv) > 1:
        arg=int(sys.argv[1])
    else:
        arg=0
    main = Optics(arg)
    main.show()
    sys.exit(app.exec_())
